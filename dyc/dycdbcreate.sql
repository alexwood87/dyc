CREATE Table Users(UserId int primary key identity(1,1),
Email NVarchar(1000) not null, Pin nvarchar(100) not null)

CREATE Table Members(MemberId int primary key identity(1,1),
Email NVarchar(1000) not null, Pin nvarchar(100) not null, IsDisabled bit not null default(0),
ProfileImage Image, FirstName nvarchar(1000), LastName nvarchar(1000), MiddleName nvarchar(200),
MemberCode nvarchar(1000)
)

CREATE TABLE CommodoreNotes(NoteId int primary key identity(1,1),
Note nvarchar(100), CreatedDate DateTime not null)

CREATE TABLE CalendarEvents(EventId bigint primary key identity(1,1),
EventName nvarchar(100) not null, EventDescription NVarchar(1000) not null,
StartDate Datetime not null, EndDate Datetime not null)

CREate Table Reservations(ReservationId bigint primary key identity(1,1),
NumberOfGuests int not null, ReservationbDate Datetime not null,
ReservationName NVARCHAR(100) not null)

CREATE TABLE MenuItems(MenuItemId int primary key identity(1,1),
ItemName Nvarchar(200) not null, Price decimal(10,2))
ALTER TABLE MenuItems
ADD ItemDescription nvarchar(1000)
ALTER TABLE MenuItems
Add IsActive bit not null
CREATE TABLE OnlineOrders(OrderId bigint primary key identity(1,1),
OrderName nvarchar(200) not null, CreatedDate Datetime not null default(Getdate()),
OrderDate datetime not null default(Getdate()),
Phone nvarchar(100) not null, [Address1] nvarchar(500),
[Address2] nvarchar(500), City nvarchar(200), ZipCode nvarchar(50),
[StateOrProvince] nvarchar(100), Dock nvarchar(100), SlipNumber nvarchar(100), VesselName nvarchar(1000))

ALTER TABLE OnlineOrders
Add IsInvalid bit not null default(0)
ALTER TABLE MenuItems
Add Picture Image
CREATE TABLE FoodOrdersToMenuItems(FoodOrdersToMenuItemId bigint primary key identity(1,1),
OrderId bigint not null, MenuItemId int not null, 
foreign key(OrderId) references OnlineOrders(OrderId),
foreign key(MenuItemId) references MenuItems(MenuItemId))

CREATE TABLE ClubOfficers(OfficerId int primary key identity(1,1),
MemberId int not null, Title nvarchar(1000) not null,
TermStart DateTime not null default(GETDATE()), 
TermEnd DateTime, foreign key(MemberId) references Members(MemberId))

CREATE TABLE MemberImages(ImageId int primary key identity(1,1),
MemberImage Image not null, MemberId int not null,
foreign key(MemberId) references Members(MemberId)) 
 
 CREATE TABLE NewsLetter(LetterId int primary key identity(1,1),
 Title nvarchar(100) not null, CreatedDate Datetime default(GETDATE()),
 Content nvarchar(MAX))

 CREATE TABLE ShopOrders(OrderId bigint primary key identity(1,1),
OrderName nvarchar(200) not null, CreatedDate Datetime not null default(Getdate()),
OrderDate datetime not null default(Getdate()),
Phone nvarchar(100) not null, [Address1] nvarchar(500),
[Address2] nvarchar(500), City nvarchar(200), ZipCode nvarchar(50),
[StateOrProvince] nvarchar(100), Dock nvarchar(100), SlipNumber nvarchar(100), VesselName nvarchar(1000))
ALTER TABLE ShopOrders
Add IsInvalid bit not null default(0)
CREATE TABLE ShopItems(ShopItemId int primary key identity(1,1),
ItemName nvarchar(100) not null, IsActive bit not null,
ItemDescription nvarchar(1000) not null, Price decimal(10,2) not null
)
ALTER TABLE ShopItems
Add Picture Image
CREATE TABLE ShopOrdersToShopItems(ShopOrdersToShopItemId bigint primary key identity(1,1),
OrderId bigint not null, ShopItemId int not null,
foreign key(OrderId) references ShopOrders(OrderId),
foreign key(ShopItemId) references ShopItems(ShopItemId))
