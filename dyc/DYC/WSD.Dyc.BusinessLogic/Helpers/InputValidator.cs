﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;
using WSD.Dyc.Domain.Models;

namespace WSD.Dyc.BusinessLogic.Helpers
{
    public class InputValidator
    {
        public static bool ValidateEmail(string email)
        {
            try
            {
                new MailAddress(email);
                return true;
            }
            catch(Exception ex)
            {
                return false;
            }
        }
        public static MemberDueModel ValidateMemberDues(MemberDueModel dues)
        {
            if(dues.AmountPaid <= 0)
            {
                throw new ArgumentException("The Member Dues Must Be A Positive Number");
            }
            return
                dues;
        }
        public static PaymentModel ValidatePayment(PaymentModel pay)
        {
            if(pay.IsPaypal)
            {
                return
                    pay;
            }
            if(string.IsNullOrEmpty(pay.NameOnCard))
            {
                throw new ArgumentException("The Name On The Card Is Required");
            }
            int i =0;
            int j = 0;
            DateTime dt = DateTime.UtcNow.AddHours(-6);
            var yearInt = dt.Year;
            if(!int.TryParse(pay.ExperationYear, out i) || !int.TryParse(pay.ExperationMonth, out j) || yearInt > i  ||(yearInt == i && j < dt.Month) )
            {
                throw new ArgumentException("Payment Date Is Invalid");
            }
            if(string.IsNullOrEmpty(pay.CardZipCode))
            {
                throw new ArgumentException("Card Zip Code Is Required");
            }
            if(string.IsNullOrEmpty(pay.CardType))
            {
                throw new ArgumentException("Card Type Is Required");
            }
            if(string.IsNullOrEmpty(pay.CreditCardNumber))
            {
                throw new ArgumentException("Credit Card Number Is Required");
            }
            if(string.IsNullOrEmpty(pay.CVVCode))
            {
                throw new ArgumentException("CVV Code Is Required");
            }
            return
                pay;
        }
        public static UserModel ValidateUser(UserModel user)
        {
            if(!ValidateEmail(user.Email))
            {
                throw new ArgumentException("Email Is Not Correctly Formatted");
            }
            if(!string.IsNullOrEmpty(user.Pin))
            {
                user.Pin = Encryptor.Encrypt(user.Pin);
            }
            return
                user;
        }
        public static MemberModel ValidateMember(MemberModel member)
        {
            if(!ValidateEmail(member.Email))
            {
                throw new ArgumentException("Member Email Is Invalid");
            }
            if(string.IsNullOrEmpty(member.LastName) || string.IsNullOrEmpty(member.FirstName))
            {
                throw new ArgumentException("Member Name Is Invalid");
            }
            if (!string.IsNullOrEmpty(member.Pin))
            {
                member.Pin = Encryptor.Encrypt(member.Pin);
            }
            return
                member;
        }
        public static string ConvertPhone(string phone)
        {
            if(phone == null)
            {
                return
                    phone;
            }
            var sb = new StringBuilder();
            int i = 0;
            foreach (var ch in phone)
            {
                if(int.TryParse(ch.ToString(), out i))
                {
                    sb.Append(i);
                }
            }
            return
                sb.ToString();
        } 
        public static ShopOrderModel ValidateShoppingOrder(ShopOrderModel model)
        {
            if(string.IsNullOrEmpty(model.Address1))
            {
                throw new ArgumentException("Address 1 Is Required");
            }
            if(string.IsNullOrEmpty(model.City ))
            {
                throw new ArgumentException("City Cannot Be Empty");
            }
            model.Phone = ConvertPhone(model.Phone);
            if(string.IsNullOrEmpty(model.Phone))
            {
                throw new ArgumentException("Phone Cannot Be Empty");
            }
            if(string.IsNullOrEmpty(model.ZipCode))
            {
                throw new ArgumentException("Zip Code Cannot Be Empty");
            }
            return
                model;
        }
        public static OnlineOrderModel ValidateOnlineOrder(OnlineOrderModel model)
        {
            if (string.IsNullOrEmpty(model.SlipNumber) && string.IsNullOrEmpty(model.VesselName) && string.IsNullOrEmpty(model.Dock))
            {
                if (string.IsNullOrEmpty(model.Address1))
                {
                    throw new ArgumentException("Address 1 Is Required");
                }
                if (string.IsNullOrEmpty(model.City))
                {
                    throw new ArgumentException("City Cannot Be Empty");
                }
                model.Phone = ConvertPhone(model.Phone);
                if (string.IsNullOrEmpty(model.Phone))
                {
                    throw new ArgumentException("Phone Cannot Be Empty");
                }
                if (string.IsNullOrEmpty(model.ZipCode))
                {
                    throw new ArgumentException("Zip Code Cannot Be Empty");
                }
            }
            else
            {
                if (string.IsNullOrEmpty(model.SlipNumber) || string.IsNullOrEmpty(model.VesselName) || string.IsNullOrEmpty(model.Dock))
                {
                    throw new ArgumentException("Online Orders Require Dock Number, Slip Number And Vessel Name If An Address Is Not Provided");
                }
                }
            return
                model;
        }
        public static ShopItemModel ValidateShopItem(ShopItemModel item)
        {
            if(string.IsNullOrEmpty(item.Name))
            {
                throw new ArgumentException("Item Name Cannot Be Empty");
            }
            if(string.IsNullOrEmpty(item.Description))
            {
                throw new ArgumentException("Item Description Cannot Be Empty");

            }
            if (item.Price <= 0)
            {
                throw new ArgumentException("Price Must Be Greater Than Zero");
            }
            return item;
        }
        public static ReservationModel ValidateReservation(ReservationModel model)
        {
            if(string.IsNullOrEmpty(model.GroupName))
            {
                throw new ArgumentException("The Group Name Cannot Be Empty");
            }
            if(model.NumberOfGuests <= 0)
            {
                throw new ArgumentException("The Number Of Guests Must Be Greater Than 0");
            }
            if(model.ReservationDate == DateTime.MinValue)
            {
                throw new ArgumentException("The Reservation Date Is Invalid");
            }
            if(model.CreatedDate == DateTime.MinValue)
            {
                model.CreatedDate = DateTime.UtcNow.AddHours(-6);
            }
            return
                model;
        }
        public static NewsLetterModel ValidateNewsLetter(NewsLetterModel model)
        {
            if(string.IsNullOrEmpty(model.Content))
            {
                throw new ArgumentException("The News LEtter Cannot Be Empty");
            }
            if(string.IsNullOrEmpty(model.Title))
            {
                throw new ArgumentException("The News Letter Title Cannot Be Empty");
            }
            if(model.CreatedDate == DateTime.MinValue)
            {
                model.CreatedDate = DateTime.UtcNow.AddHours(-6);
            }
            return
                model;
        }
        public static CalendarEventModel ValidateEvent(CalendarEventModel model)
        {
            if (string.IsNullOrEmpty(model.Content))
            {
                throw new ArgumentException("The Event Content Cannot Be Empty");
            }
            if(string.IsNullOrEmpty(model.Title))
            {
                throw new ArgumentException("The Event Title Cannot Be Empty");
            }
            if(model.StartDate == DateTime.MinValue)
            {
                throw new ArgumentException("The Start Date Is Invalid");
            }
            if(model.EndDate == DateTime.MinValue)
            {
                throw new ArgumentException("The end Date Is Invalid"); 
            }
            return
                model;
        }
        public static CommodoreNoteModel ValidateCommodoreNote(CommodoreNoteModel model)
        {
            if(string.IsNullOrEmpty(model.Note))
            {
                throw new ArgumentException("The Note Cannot Be Empty");
            }
            if(model.CreatedDate == DateTime.MinValue)
            {
                model.CreatedDate = DateTime.UtcNow.AddHours(-6);
            }
            return model;
        }
        public static MenuItemModel ValidateMenuItem(MenuItemModel item)
        {
            if (string.IsNullOrEmpty(item.Name))
            {
                throw new ArgumentException("Item Name Cannot Be Empty");
            }
            if (string.IsNullOrEmpty(item.Description))
            {
                throw new ArgumentException("Item Description Cannot Be Empty");

            }
            if (item.Price <= 0)
            {
                throw new ArgumentException("Price Must Be Greater Than Zero");
            }
            return item;
        }
    }
}
