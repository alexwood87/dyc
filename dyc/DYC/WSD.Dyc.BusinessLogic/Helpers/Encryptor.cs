﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WSD.Dyc.BusinessLogic.Helpers
{
    public class Encryptor
    {
        public static string Encrypt(string str)
        {
            byte[] data = Encoding.UTF8.GetBytes(str);
            data = new System.Security.Cryptography.SHA256Managed().ComputeHash(data);
            string hash = Encoding.UTF8.GetString(data);
            return hash;
        }
    }
}
