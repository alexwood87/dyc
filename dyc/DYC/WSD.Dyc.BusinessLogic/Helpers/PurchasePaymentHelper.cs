﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WSD.Dyc.Domain.Models;

namespace WSD.Dyc.BusinessLogic.Helpers
{
    public class PurchasePaymentHelper
    {
        public static int RESULT_OK = 1;
        public static int RESULT_FAILURE = 2;

        public int MakeShopOrderPayment(ShopOrderModel order, PaymentModel payment)
        {
            return RESULT_OK;
        }
        public int MakeOnlineFoodPayment(OnlineOrderModel order, PaymentModel payment)
        {
            return RESULT_OK;
        }
        public int PayDues(MemberDueModel dues, PaymentModel payment)
        {
            return RESULT_OK;
        }
    }
}
