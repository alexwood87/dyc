﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WSD.Dyc.Core.Contracts;
using WSD.Dyc.Domain.Models;
using WSD.Dyc.Domain.UnitsOfWork;
using WSD.Dyc.Infrastructure.UnitsOfWork;
using WSD.Dyc.BusinessLogic.Helpers;
namespace WSD.Dyc.BusinessLogic
{
    public class AppBusinessLogic : IAppBusinessLogic
    {
        private IUnitOfWork _unitOfWork;
        public AppBusinessLogic(string connString)
        {
            _unitOfWork = new UnitOfWork(connString);
        }
        public long PayDues(MemberDueModel model, PaymentModel payment)
        {
            payment = InputValidator.ValidatePayment(payment);
            model = InputValidator.ValidateMemberDues(model);
            var helper = new PurchasePaymentHelper();
            if(helper.PayDues(model, payment) == PurchasePaymentHelper.RESULT_OK)
            {
                return
                    _unitOfWork.MemberRepository.PayDues(model);
            }
            return
                -1;
        }
        public long AddOnlineOrder(OnlineOrderModel model, PaymentModel payment)
        {
            model = InputValidator.ValidateOnlineOrder(model);
            if (payment != null)
            {
                payment = InputValidator.ValidatePayment(payment);
            }
            var helper = new PurchasePaymentHelper();
            if(payment == null || helper.MakeOnlineFoodPayment(model, payment) == PurchasePaymentHelper.RESULT_OK)
            {
                return _unitOfWork.ShoppingRepository.AddOnlineOrder(model);
            }
            return -1;
        }

        public int AddOrUpdateImage(ImageModel model)
        {
            return _unitOfWork.ImageRepository.AddOrUpdateImage(model);
        }

        public int AddOrUpdateMembers(MemberModel model)
        {
            model = InputValidator.ValidateMember(model);
          
            return _unitOfWork.MemberRepository.AddOrUpdateMembers(model);
        }

        public int AddOrUpdateOfficer(ClubOfficerModel model)
        {
            return
                _unitOfWork.MemberRepository.AddOrUpdateOfficer(model);
        }

        public long AddShoppingOrder(ShopOrderModel model, PaymentModel payment)
        {
            model = InputValidator.ValidateShoppingOrder(model);
            if (payment != null)
            {
                payment = InputValidator.ValidatePayment(payment);
            }
            var helper = new PurchasePaymentHelper();
            if(payment == null || helper.MakeShopOrderPayment(model, payment) == PurchasePaymentHelper.RESULT_OK)
            {
                return
                    _unitOfWork.ShoppingRepository.AddShoppingOrder(model);
            }
            return
                -1;
        }

        public int AddUpdateCommodoreNote(CommodoreNoteModel note, string fromEmail, string userSystemEmail, string userSystemPassword)
        {
            note = InputValidator.ValidateCommodoreNote(note);
           
            var retVal =
                _unitOfWork.NotesAndNewsRepository.AddUpdateCommodoreNote(note);
            SendEmails("Commodore Note Of " + note.CreatedDate.ToString("MM/dd/yyyy"), note.Note, fromEmail, userSystemEmail, userSystemPassword);
            return
                retVal;
        }

        public long AddUpdateEvent(CalendarEventModel model)
        {
            model = InputValidator.ValidateEvent(model);
            return
                _unitOfWork.EventRepository.AddUpdateEvent(model);
        }

        public int AddUpdateMenuItem(MenuItemModel item)
        {
            item = InputValidator.ValidateMenuItem(item);
            return
                _unitOfWork.ShoppingRepository.AddUpdateMenuItem(item);
        }
        private void SendEmails(string subject, string content, string from, string userEmail, string userPassword)
        {
            int total;
            var members = _unitOfWork.MemberRepository.SearchMembers(new MemberSearchModel
            {
                PageSize = int.MaxValue,
                HasOptedOut = false
            }, out total);
            foreach (var mem in members)
            {
                WSDLLC.App.Common.CommonFunctions.SendEmail(from, mem.Email, content, subject, userEmail, userPassword);
            }
        }
        public int AddUpdateNewsLetter(NewsLetterModel model, string fromEmail, string userSystemEmail, string userSystemPassword)
        {
            model = InputValidator.ValidateNewsLetter(model);

            var retVal =
                _unitOfWork.NotesAndNewsRepository.AddUpdateNewsLetter(model);
            SendEmails(model.Title, model.Content, fromEmail, userSystemEmail, userSystemPassword);
            
            return
                retVal;
        }

        public long AddUpdateReservations(ReservationModel model)
        {
            model = InputValidator.ValidateReservation(model);
            return
                _unitOfWork.ShoppingRepository.AddUpdateReservations(model);
        }

        public int AddUpdateShoppingItem(ShopItemModel item)
        {
            item = InputValidator.ValidateShopItem(item);
            return
                _unitOfWork.ShoppingRepository.AddUpdateShoppingItem(item);
        }

        public List<ImageModel> GetAllImages(int pageIndex, int pageSize, out int total)
        {
            return
                _unitOfWork.ImageRepository.GetAllImages(pageIndex, pageSize, out total);
        }

        public List<MenuItemModel> GetAllMenuItems(int pageIndex, int pageSize, out int total)
        {
            return
                _unitOfWork.ShoppingRepository.GetAllMenuItems(pageIndex, pageSize, out total);
        }

        public List<ShopItemModel> GetAllShopItems(int pageIndex, int pageSize, out int total)
        {
            return
                _unitOfWork.ShoppingRepository.GetAllShopItems(pageIndex, pageSize, out total);
        }
        public List<MenuItemModel> GetAllMenuItemsAdmin(int pageIndex, int pageSize, out int total)
        {
            return
                _unitOfWork.ShoppingRepository.GetAllMenuItemsAdmin(pageIndex, pageSize, out total);
        }

        public List<ShopItemModel> GetAllShopItemsAdmin(int pageIndex, int pageSize, out int total)
        {
            return
                _unitOfWork.ShoppingRepository.GetAllShopItemsAdmin(pageIndex, pageSize, out total);
        }

        public List<UserModel> GetAllUsers()
        {
            return
                _unitOfWork.UserRepository.GetAllUsers();
        }

        public List<ClubOfficerModel> GetOfficers()
        {
            return
                _unitOfWork.MemberRepository.GetOfficers();
        }

        public void InvalidateOnlineOrder(long orderId)
        {
            _unitOfWork.ShoppingRepository.InvalidateOnlineOrder(orderId);
        }

        public void InvalidateShopOrder(long orderId)
        {
            _unitOfWork.ShoppingRepository.InvalidateShopOrder(orderId);
        }

        public ClubOfficerModel IsClubOfficer(int memberId, DateTime termDate)
        {
            return
                _unitOfWork.MemberRepository.IsClubOfficer(memberId, termDate);
        }

        public void RemoveUser(int userId)
        {
            _unitOfWork.UserRepository.RemoveUser(userId);
        }

        public List<CommodoreNoteModel> SearchCommodoreNotes(int? id, DateTime? currentDate, int pageIndex, int pageSize)
        {
            return
                _unitOfWork.NotesAndNewsRepository.SearchCommodoreNotes(id, currentDate, pageIndex, pageSize);
        }

        public List<CalendarEventModel> SearchEvents(EventSearchModel search)
        {
            return
                _unitOfWork.EventRepository.SearchEvents(search);
        }

        public List<MemberModel> SearchMembers(MemberSearchModel search, out int total)
        {
            return
                _unitOfWork.MemberRepository.SearchMembers(search, out total);
        }

        public List<NewsLetterModel> SearchNewsLetters(int? id, DateTime? currentDate, int pageIndex, int pageSize)
        {
            return
                _unitOfWork.NotesAndNewsRepository.SearchNewsLetters(id, currentDate, pageIndex, pageSize);
        }

        public List<OnlineOrderModel> SearchOnlineOrders(ShoppingSearchModel search)
        {
            return
                _unitOfWork.ShoppingRepository.SearchOnlineOrders(search);
        }

        public List<ReservationModel> SearchReservations(ReservationSearch search)
        {
            return
                _unitOfWork.ShoppingRepository.SearchReservations(search);
        }

        public List<ShopOrderModel> SearchShoppingOrders(ShoppingSearchModel search)
        {
            return
                _unitOfWork.ShoppingRepository.SearchShoppingOrders(search);
        }

        public MemberModel ValidateMember(string email, string pin)
        {
            pin = Encryptor.Encrypt(pin);
            return
                _unitOfWork.MemberRepository.ValidateMember(email, pin);
        }

        public UserModel ValidateUser(string email, string pin)
        {
            pin = Encryptor.Encrypt(pin);
            return
                _unitOfWork.UserRepository.ValidateUser(email, pin);
        }

        public int AddUpdateUser(UserModel u)
        {
            u = InputValidator.ValidateUser(u);
            return
                _unitOfWork.UserRepository.AddUpdateUser(u);
        }

        public List<OnlineOrderModel> FindBarGrillOrders(DateTime? dateFulfilled)
        {
            return
                _unitOfWork.ShoppingRepository.FindBarGrillOrders(dateFulfilled);
        }

        public List<ShopOrderModel> FindShoppingOrders(DateTime? dateFulfilled)
        {
            return
                _unitOfWork.ShoppingRepository.FindShoppingOrders(dateFulfilled);
        }
    }
}
