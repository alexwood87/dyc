﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WSD.Dyc.Domain.Models;

namespace WSD.Dyc.Core.Contracts
{
    public interface IAppBusinessLogic
    {
        long AddUpdateEvent(CalendarEventModel model);
        List<CalendarEventModel> SearchEvents(EventSearchModel search);
        List<ImageModel> GetAllImages(int pageIndex, int pageSize, out int total);
        int AddOrUpdateImage(ImageModel model);

        List<MemberModel> SearchMembers(MemberSearchModel search, out int total);
        MemberModel ValidateMember(string email, string pin);
        int AddOrUpdateMembers(MemberModel model);
        List<ClubOfficerModel> GetOfficers();
        ClubOfficerModel IsClubOfficer(int memberId, DateTime termDate);
        int AddOrUpdateOfficer(ClubOfficerModel model);
        List<CommodoreNoteModel> SearchCommodoreNotes(int? id, DateTime? currentDate, int pageIndex, int pageSize);
        int AddUpdateCommodoreNote(CommodoreNoteModel note, string fromEmail, string userSystemEmail, string userSystemPassword);
        List<NewsLetterModel> SearchNewsLetters(int? id, DateTime? currentDate, int pageIndex, int pageSize);
        int AddUpdateNewsLetter(NewsLetterModel model, string fromEmail, string userSystemEmail, string userSystemPassword);
        long AddShoppingOrder(ShopOrderModel model, PaymentModel payment);
        long AddOnlineOrder(OnlineOrderModel model, PaymentModel payment);
        long PayDues(MemberDueModel model, PaymentModel payment);
        void InvalidateOnlineOrder(long orderId);
        int AddUpdateUser(UserModel u);
        void InvalidateShopOrder(long orderId);
        List<OnlineOrderModel> SearchOnlineOrders(ShoppingSearchModel search);
        List<ShopOrderModel> SearchShoppingOrders(ShoppingSearchModel search);
        int AddUpdateMenuItem(MenuItemModel item);
        int AddUpdateShoppingItem(ShopItemModel item);
        List<ShopItemModel> GetAllShopItems(int pageIndex, int pageSize, out int total);
        List<MenuItemModel> GetAllMenuItems(int pageIndex, int pageSize, out int total);
        List<ShopItemModel> GetAllShopItemsAdmin(int pageIndex, int pageSize, out int total);
        List<MenuItemModel> GetAllMenuItemsAdmin(int pageIndex, int pageSize, out int total);
        List<ReservationModel> SearchReservations(ReservationSearch search);
        long AddUpdateReservations(ReservationModel model);
        UserModel ValidateUser(string email, string pin);
        List<UserModel> GetAllUsers();
        void RemoveUser(int userId);
        List<OnlineOrderModel> FindBarGrillOrders(DateTime? dateFulfilled);
        List<ShopOrderModel> FindShoppingOrders(DateTime? dateFulfilled);
    }
}
