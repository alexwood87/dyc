﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;
using WSD.Dyc.Domain.Entities;
using WSD.Dyc.Domain.Models;
using WSD.Dyc.Domain.Repositories;

namespace WSD.Dyc.Infrastructure.Repositories
{
    public class ShoppingRepository : IShoppingRepository
    {
        private diverseyyachtclubEntities _context;
        public ShoppingRepository(diverseyyachtclubEntities context)
        {
            _context = context;
        }

        public long AddOnlineOrder(OnlineOrderModel model)
        {
            using (var trans = new CommittableTransaction(new TimeSpan(0, 5, 0)))
            {
                try {
                    var m = _context.OnlineOrders.SingleOrDefault(x => x.OrderId == model.OrderId);
                    if (m != null)
                    {
                        m.OrderName = model.OrderName;
                        m.OrderDate = model.OrderDate;
                        m.Phone = model.Phone;
                        m.SlipNumber = model.SlipNumber;
                        m.StateOrProvince = model.StateOrProvidence;
                        m.IsInvalid = false;
                        m.City = model.City;
                        m.Address1 = model.Address1;
                        m.Address2 = model.Address2;
                        m.Dock = model.Dock;
                        m.VesselName = model.VesselName;
                        m.ZipCode = model.ZipCode;
                        m.DateFulfilled = model.DateFulfilled;
                        _context.SaveChanges();
                        var items = _context.FoodOrdersToMenuItems.Where(x => x.OrderId == model.OrderId);
                        foreach (var it in items.ToList())
                        {
                            var food = _context.FoodOrdersToMenuItems.Single(x => x.FoodOrdersToMenuItemId == it.FoodOrdersToMenuItemId);
                            _context.FoodOrdersToMenuItems.Remove(food);
                            _context.SaveChanges();
                        }
                        foreach (var it2 in model.Items)
                        {
                            var fotmi = new FoodOrdersToMenuItem
                            {
                                MenuItemId = it2.MenuItemId,
                                OrderId = m.OrderId
                            };
                            _context.FoodOrdersToMenuItems.Add(fotmi);
                            _context.SaveChanges();
                        }
                        trans.Commit();
                        return
                            m.OrderId;
                    }
                    var order = new OnlineOrder
                    {
                        Address1 = model.Address1,
                        Address2 = model.Address2,
                        City = model.City,
                        CreatedDate = model.CreatedDate,
                        Dock = model.Dock,
                        IsInvalid = false,
                        OrderDate = model.OrderDate,
                        OrderName = model.OrderName,
                        Phone = model.Phone,
                        SlipNumber = model.SlipNumber,
                        StateOrProvince = model.StateOrProvidence,
                        VesselName = model.VesselName,
                        ZipCode = model.ZipCode,
                        DateFulfilled = model.DateFulfilled
                    };
                    _context.OnlineOrders.Add(order);
                    _context.SaveChanges();
                    foreach (var it1 in model.Items)
                    {
                        var fotmi2 = new FoodOrdersToMenuItem
                        {
                            MenuItemId = it1.MenuItemId,
                            OrderId = order.OrderId
                        };
                        _context.FoodOrdersToMenuItems.Add(fotmi2);
                        _context.SaveChanges();
                    }
                    trans.Commit();
                    return
                        order.OrderId;

                }
                catch(Exception ex)
                {
                    trans.Rollback(ex);
                    return
                        -1;
                }
           }
           
        }

        public long AddShoppingOrder(ShopOrderModel model)
        {
            using (var trans = new CommittableTransaction(new TimeSpan(0, 5, 0)))
            {
                try
                {
                    var m = _context.ShopOrders.SingleOrDefault(x => x.OrderId == model.OrderId);
                    if (m != null)
                    {
                        m.OrderName = model.OrderName;
                        m.OrderDate = model.OrderDate;
                        m.Phone = model.Phone;
                        m.SlipNumber = model.SlipNumber;
                        m.StateOrProvince = model.StateOrProvidence;
                        m.IsInvalid = false;
                        m.City = model.City;
                        m.Address1 = model.Address1;
                        m.Address2 = model.Address2;
                        m.Dock = model.Dock;
                        m.VesselName = model.VesselName;
                        m.ZipCode = model.ZipCode;
                        m.DateFulfilled = model.DateFulfilled;
                        _context.SaveChanges();
                        var items = _context.ShopOrdersToShopItems.Where(x => x.OrderId == model.OrderId);
                        foreach (var it in items.ToList())
                        {
                            var item = _context.ShopOrdersToShopItems.Single(x => x.ShopOrdersToShopItemId == it.ShopOrdersToShopItemId);
                            _context.ShopOrdersToShopItems.Remove(item);
                            _context.SaveChanges();
                        }
                        foreach (var it2 in model.Items)
                        {
                            var fotmi = new ShopOrdersToShopItem
                            {
                                ShopItemId = it2.ShopItemId,
                                OrderId = m.OrderId
                            };
                            _context.ShopOrdersToShopItems.Add(fotmi);
                            _context.SaveChanges();
                        }
                        trans.Commit();
                        return
                            m.OrderId;
                    }
                    var order = new ShopOrder
                    {
                        Address1 = model.Address1,
                        Address2 = model.Address2,
                        City = model.City,
                        CreatedDate = model.CreatedDate,
                        Dock = model.Dock,
                        IsInvalid = false,
                        OrderDate = model.OrderDate,
                        OrderName = model.OrderName,
                        Phone = model.Phone,
                        SlipNumber = model.SlipNumber,
                        StateOrProvince = model.StateOrProvidence,
                        VesselName = model.VesselName,
                        ZipCode = model.ZipCode,
                        DateFulfilled = model.DateFulfilled
                    };
                    _context.ShopOrders.Add(order);
                    _context.SaveChanges();
                    foreach (var it2 in model.Items)
                    {
                        var fotmi = new ShopOrdersToShopItem
                        {
                            ShopItemId = it2.ShopItemId,
                            OrderId = m.OrderId
                        };
                        _context.ShopOrdersToShopItems.Add(fotmi);
                        _context.SaveChanges();
                    }
                    trans.Commit();
                    return
                        order.OrderId;

                }
                catch (Exception ex)
                {
                    trans.Rollback(ex);
                    return
                        -1;
                }
            }
        }
        public List<OnlineOrderModel> FindBarGrillOrders(DateTime? dateFulfilled)
        {
            return
              _context.OnlineOrders.Where(x => x.DateFulfilled == dateFulfilled)
              .Select(x => new OnlineOrderModel
              {
                  Address1 = x.Address1,
                  Address2 = x.Address2,
                  City = x.City,
                  CreatedDate = x.CreatedDate,
                  DateFulfilled = x.DateFulfilled,
                  Dock = x.Dock,
                  OrderDate = x.OrderDate,
                  OrderId = x.OrderId,
                  OrderName = x.OrderName,
                  Phone = x.Phone,
                  SlipNumber = x.SlipNumber,
                  StateOrProvidence = x.StateOrProvince,
                  VesselName = x.VesselName,
                  ZipCode = x.ZipCode,
                  Items = x.FoodOrdersToMenuItems.Select(y => new MenuItemModel
                  {
                      Description = y.MenuItem.ItemDescription,
                      IsActive = y.MenuItem.IsActive,
                      Name = y.MenuItem.ItemName,
                      Picture = y.MenuItem.Picture,
                      Price = y.MenuItem.Price.Value,
                      MenuItemId = y.MenuItemId
                  }).ToList()
              }).ToList();
        }
        public List<ShopOrderModel> FindShoppingOrders(DateTime? dateFulfilled)
        {
            return
                _context.ShopOrders.Where(x => x.DateFulfilled == dateFulfilled)
                .Select(x => new ShopOrderModel
                {
                    Address1 = x.Address1,
                    Address2 = x.Address2,
                    City = x.City,
                    CreatedDate = x.CreatedDate,
                    DateFulfilled = x.DateFulfilled,
                    Dock = x.Dock,
                    OrderDate = x.OrderDate,
                    OrderId = x.OrderId,
                    OrderName = x.OrderName,
                    Phone = x.Phone,
                    SlipNumber = x.SlipNumber,
                    StateOrProvidence = x.StateOrProvince,
                    VesselName = x.VesselName,
                    ZipCode = x.ZipCode,
                    Items = x.ShopOrdersToShopItems.Select(y => new ShopItemModel
                    {
                        Description = y.ShopItem.ItemDescription,
                        IsActive = y.ShopItem.IsActive,
                        Name = y.ShopItem.ItemName,
                        Picture = y.ShopItem.Picture,
                        Price = y.ShopItem.Price,
                        ShopItemId = y.ShopItemId
                    }).ToList()
                }).ToList();
        }
        public int AddUpdateMenuItem(MenuItemModel item)
        {
            var m = _context.MenuItems.SingleOrDefault(x => x.MenuItemId == item.MenuItemId);
            if (m != null)
            {
                m.ItemName = item.Name;
                m.Price = item.Price;
                m.Category = item.Category;
                m.ItemDescription = item.Description;
                m.IsActive = item.IsActive;
                if (item.Picture != null && item.Picture.Length > 0)
                {
                    m.Picture = item.Picture;
                }
                //_context.MenuItems.Attach(m);
                _context.SaveChanges();
                return
                    item.MenuItemId;
            }
            m = new MenuItem
            {
                IsActive = item.IsActive,
                Price = item.Price,
                ItemName = item.Name,
                Picture = item.Picture,
                ItemDescription = item.Description,
                Category = item.Category                
            };
            _context.MenuItems.Add(m);
            _context.SaveChanges();
            return
                m.MenuItemId;
        }

        public long AddUpdateReservations(ReservationModel model)
        {
            var m = _context.Reservations.SingleOrDefault(c => c.ReservationId == model.ReservationId);
            if(m != null)
            {
                m.ReservationName = model.GroupName;
                m.ReservationbDate = model.ReservationDate;
                m.NumberOfGuests = model.NumberOfGuests;
                _context.SaveChanges();
                return
                    m.ReservationId;
            }
            m = new Reservation
            {
                ReservationbDate = model.ReservationDate,
                NumberOfGuests = model.NumberOfGuests,
                ReservationName = model.GroupName
            };
            _context.Reservations.Add(m);
            _context.SaveChanges();
            return
                m.ReservationId;
        }

        public int AddUpdateShoppingItem(ShopItemModel item)
        {

            var m = _context.ShopItems.SingleOrDefault(x => x.ShopItemId == item.ShopItemId);
            if (m != null)
            {
                m.ItemName = item.Name;
                m.Price = item.Price;
                m.ItemDescription = item.Description;
                m.IsActive = item.IsActive;
                if (item.Picture != null && item.Picture.Length > 0)
                {
                    m.Picture = item.Picture;
                }
                _context.SaveChanges();
                return
                    item.ShopItemId;
            }
            m = new ShopItem
            {
                IsActive = item.IsActive,
                Price = item.Price,
                ItemName = item.Name,
                ItemDescription = item.Description,
                Picture = item.Picture
            };
            _context.ShopItems.Add(m);
            _context.SaveChanges();
            return
                m.ShopItemId;
        }

        public List<MenuItemModel> GetAllMenuItems(int pageIndex, int pageSize, out int total)
        {
            var query = _context.MenuItems.Where(x => x.IsActive).AsQueryable();
            total = query.Count();
            return
                query.OrderBy(x => x.ItemName).Skip(pageIndex * pageSize).ToList()
                    .Select(x => new MenuItemModel
                    {
                        Description = x.ItemDescription,
                        IsActive = x.IsActive,
                        MenuItemId = x.MenuItemId,
                        Category = x.Category,
                        Name = x.ItemName,
                        Price = x.Price.GetValueOrDefault(0),
                        Picture = x.Picture
                    }).ToList();
        }

        public List<MenuItemModel> GetAllMenuItemsAdmin(int pageIndex, int pageSize, out int total)
        {
            var query = _context.MenuItems.AsQueryable();
            total = query.Count();
            return
                query.OrderBy(x => x.ItemName).Skip(pageIndex * pageSize).ToList()
                    .Select(x => new MenuItemModel
                    {
                        Description = x.ItemDescription,
                        IsActive = x.IsActive,
                        MenuItemId = x.MenuItemId,
                        Name = x.ItemName,
                        Category = x.Category,
                        Price = x.Price.GetValueOrDefault(0),
                        Picture = x.Picture
                    }).ToList();
        }
        public List<ShopItemModel> GetAllShopItems(int pageIndex, int pageSize, out int total)
        {
            var query = _context.ShopItems.Where(x => x.IsActive).AsQueryable();
            total = query.Count();
            return
                query.OrderBy(x => x.ItemName).Skip(pageIndex * pageSize).ToList()
                    .Select(x => new ShopItemModel
                    {
                        Description = x.ItemDescription,
                        IsActive = x.IsActive,
                        ShopItemId = x.ShopItemId,
                        Name = x.ItemName,
                        Price = x.Price,
                        Picture = x.Picture
                    }).ToList();
        }
        public List<ShopItemModel> GetAllShopItemsAdmin(int pageIndex, int pageSize, out int total)
        {
            var query = _context.ShopItems.AsQueryable();
            total = query.Count();
            return
                query.OrderBy(x => x.ItemName).Skip(pageIndex * pageSize).ToList()
                    .Select(x => new ShopItemModel
                    {
                        Description = x.ItemDescription,
                        IsActive = x.IsActive,
                        ShopItemId = x.ShopItemId,
                        Name = x.ItemName,
                        Price = x.Price,
                        Picture = x.Picture
                    }).ToList();
        }
        public void InvalidateOnlineOrder(long orderId)
        {
            var m = _context.OnlineOrders.SingleOrDefault(x => x.OrderId == orderId);
            if(m != null)
            {
                m.IsInvalid = true;
                _context.SaveChanges();
            }
        }

        public void InvalidateShopOrder(long orderId)
        {
            var m = _context.ShopOrders.SingleOrDefault(x => x.OrderId == orderId);
            if(m != null)
            {
                m.IsInvalid = true;
                _context.SaveChanges();
            }
        }

        public List<OnlineOrderModel> SearchOnlineOrders(ShoppingSearchModel search)
        {
            var query = _context.OnlineOrders.AsQueryable();
            if(search.OrderId != null)
            {
                query = query.Where(x => x.OrderId == search.OrderId.Value);
            }
            if(!string.IsNullOrEmpty(search.OrderName))
            {
                query = query.Where(x => x.OrderName.ToLower() == search.OrderName.ToLower());
            }
            if(search.OrderDate != null)
            {
                query = query.Where(x => x.OrderDate == search.OrderDate.Value);
            }
            return
                query.OrderByDescending(c => c.OrderDate).Skip(search.PageIndex * search.PageSize).Take(search.PageSize)
                    .ToList().Select(x => new OnlineOrderModel
                    {
                        Address1 = x.Address1,
                        Address2 = x.Address2,
                        City = x.City,
                        CreatedDate = x.CreatedDate,
                        Dock = x.Dock,
                        OrderDate = x.OrderDate,
                        OrderId = x.OrderId,
                        OrderName = x.OrderName,
                        Phone = x.Phone,
                        SlipNumber = x.SlipNumber,
                        StateOrProvidence = x.StateOrProvince,
                        VesselName = x.VesselName,
                        ZipCode = x.ZipCode,
                        Items = x.FoodOrdersToMenuItems.Select(y => new MenuItemModel
                        {
                            Description = y.MenuItem.ItemDescription,
                            IsActive = y.MenuItem.IsActive,
                            MenuItemId = y.MenuItemId,
                            Name = y.MenuItem.ItemName,
                            Price = y.MenuItem.Price.Value
                        }).ToList()
                    }).ToList();
        }

        public List<ReservationModel> SearchReservations(ReservationSearch search)
        {
            var query = _context.Reservations.AsQueryable();
            if(search.ReservationId != null)
            {
                query = query.Where(x => x.ReservationId == search.ReservationId.Value);
            }
            if(search.FromDate != null)
            {
                query = query.Where(x => x.ReservationbDate >= search.FromDate.Value);
            }
            if(search.ToDate != null)
            {
                query = query.Where(x => x.ReservationbDate <= search.ToDate.Value);
            }
            if(!string.IsNullOrEmpty(search.GroupName))
            {
                query = query.Where(x => x.ReservationName.ToLower() == search.GroupName.ToLower());
            }
            return
                query.OrderBy(x => x.ReservationbDate).ThenBy(x => x.ReservationName).ThenBy(x => x.NumberOfGuests)
                .ToList().Select(x => new ReservationModel
                {
                    CreatedDate = x.ReservationbDate,
                    ReservationDate = x.ReservationbDate,
                    GroupName = x.ReservationName,
                    ReservationId = x.ReservationId,
                    NumberOfGuests = x.NumberOfGuests
                }).ToList();
        }

        public List<ShopOrderModel> SearchShoppingOrders(ShoppingSearchModel search)
        {
            var query = _context.ShopOrders.AsQueryable();
            if (search.OrderId != null)
            {
                query = query.Where(x => x.OrderId == search.OrderId.Value);
            }
            if (!string.IsNullOrEmpty(search.OrderName))
            {
                query = query.Where(x => x.OrderName.ToLower() == search.OrderName.ToLower());
            }
            if (search.OrderDate != null)
            {
                query = query.Where(x => x.OrderDate == search.OrderDate.Value);
            }
            return
                query.OrderByDescending(c => c.OrderDate).Skip(search.PageIndex * search.PageSize).Take(search.PageSize)
                    .ToList().Select(x => new ShopOrderModel
                    {
                        Address1 = x.Address1,
                        Address2 = x.Address2,
                        City = x.City,
                        CreatedDate = x.CreatedDate,
                        Dock = x.Dock,
                        OrderDate = x.OrderDate,
                        OrderId = x.OrderId,
                        OrderName = x.OrderName,
                        Phone = x.Phone,
                        SlipNumber = x.SlipNumber,
                        StateOrProvidence = x.StateOrProvince,
                        VesselName = x.VesselName,
                        ZipCode = x.ZipCode,
                        Items = x.ShopOrdersToShopItems.Select(y => new ShopItemModel
                        {
                            Description = y.ShopItem.ItemDescription,
                            IsActive = y.ShopItem.IsActive,
                            ShopItemId = y.ShopItemId,
                            Name = y.ShopItem.ItemName,
                            Price = y.ShopItem.Price
                        }).ToList()
                    }).ToList();
        }
    }
}
