﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WSD.Dyc.Domain.Entities;
using WSD.Dyc.Domain.Models;
using WSD.Dyc.Domain.Repositories;

namespace WSD.Dyc.Infrastructure.Repositories
{
    public class EventRepository : IEventRepository
    {
        private diverseyyachtclubEntities _context;
        public EventRepository(diverseyyachtclubEntities context)
        {
            _context = context;
        }
        public long AddUpdateEvent(CalendarEventModel model)
        {
            if(model.EventId > 0)
            {
                var e = _context.CalendarEvents.Single(x => x.EventId == model.EventId);
                e.EventName = model.Title;
                e.EndDate = model.EndDate;
                e.StartDate = model.StartDate;
                e.EventDescription = model.Content;
                _context.SaveChanges();     
                return
                    model.EventId;
            }
            var ev = new CalendarEvent
            {
                EventName = model.Title,
                EventDescription = model.Content,
                EndDate = model.EndDate,
                StartDate = model.StartDate
            };
            _context.CalendarEvents.Add(ev);
            _context.SaveChanges();
            return
                ev.EventId;
        }

        public List<CalendarEventModel> SearchEvents(EventSearchModel search)
        {
            var query = _context.CalendarEvents.AsQueryable();
            if(search.FromDate != null)
            {
                query = query.Where(x => x.StartDate >= search.FromDate.Value);
            }
            if(search.ToDate != null)
            {
                query = query.Where(x => x.EndDate <= search.ToDate.Value);
            }
            if(search.EventId != null)
            {
                query = query.Where(x => x.EventId == search.EventId.Value);
            }
            if(!string.IsNullOrEmpty(search.Description))
            {
                query = query.Where(x => x.EventDescription.ToLower().Contains(search.Description.ToLower()));
            }
            if(!string.IsNullOrEmpty(search.Title))
            {
                query = query.Where(x => x.EventName.ToLower().Contains(search.Title.ToLower()));
            }
            return
                query.OrderByDescending(x => x.EndDate).ThenByDescending(x => x.StartDate).Skip(search.PageSize * search.PageIndex).ToList()
                    .Select(x => new CalendarEventModel {
                        Content = x.EventDescription,
                        EndDate = x.EndDate,
                        StartDate = x.StartDate,
                        EventId = x.EventId,
                        Title = x.EventName
                    }).ToList();

        }
    }
}
