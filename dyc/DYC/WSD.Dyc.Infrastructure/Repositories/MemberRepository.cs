﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;
using WSD.Dyc.Domain.Entities;
using WSD.Dyc.Domain.Models;
using WSD.Dyc.Domain.Repositories;

namespace WSD.Dyc.Infrastructure.Repositories
{
    public class MemberRepository : IMemberRepository
    {
        private diverseyyachtclubEntities _context;
        public MemberRepository(diverseyyachtclubEntities context)
        {
            _context = context;
        }
        public int AddOrUpdateMembers(MemberModel model)
        {
            if(model.MemberId > 0)
            {
                var m = _context.Members.Single(x => x.MemberId == model.MemberId);
                m.MemberCode = model.MemberCode;
                m.IsDisabled = model.IsDisabled;
                m.LastName = model.LastName;
                m.MiddleName = model.MiddleName;
                if (!string.IsNullOrEmpty(model.Pin))
                {
                    m.Pin = model.Pin;
                }
                if (model.ProfileImage != null)
                {
                    m.ProfileImage = model.ProfileImage;
                }
                m.Email = model.Email;
                m.FirstName = model.FirstName;
                m.OptOut = model.OptOut;
                _context.SaveChanges();
                return
                    model.MemberId;
            }
            var m2 = new Member
            {
                Email = model.Email,
                IsDisabled = model.IsDisabled,
                FirstName = model.FirstName,
                LastName = model.LastName,
                MemberCode = model.MemberCode,
                MiddleName = model.MiddleName,
                Pin = model.Pin,
                OptOut = model.OptOut,
                ProfileImage = model.ProfileImage
            };
            _context.Members.Add(m2);
            _context.SaveChanges();
            return
                m2.MemberId;
        }

        public int AddOrUpdateOfficer(ClubOfficerModel model)
        {
            var m = _context.ClubOfficers.SingleOrDefault(x => x.Title.ToLower() == model.Title.ToLower());
            if (m != null && (m.TermEnd == null || m.TermEnd.Value > DateTime.UtcNow.AddHours(-6)))
            {
                m.TermEnd = DateTime.UtcNow.AddHours(-6);
                _context.SaveChanges();
            }
            var c2 = new ClubOfficer
            {
                MemberId = model.Member.MemberId,
                TermEnd = model.TermEnd,
                TermStart = model.TermStart,
                Title = model.Title
            };
            _context.ClubOfficers.Add(c2);
            _context.SaveChanges();
            return
                c2.OfficerId;
        }

        public List<ClubOfficerModel> GetOfficers()
        {
            return
                _context.ClubOfficers.ToList().Select(x => new ClubOfficerModel
                {
                    OfficerId = x.OfficerId,
                    TermEnd = x.TermEnd,
                    TermStart = x.TermStart,
                    Title = x.Title,
                    Member = new MemberModel
                    {
                        Email = x.Member.Email,
                        FirstName = x.Member.FirstName,
                        IsDisabled = x.Member.IsDisabled,
                        LastName = x.Member.LastName,
                        MemberCode = x.Member.MemberCode,
                        MemberId = x.MemberId,
                        MiddleName = x.Member.MiddleName,
                        Pin = x.Member.Pin,
                        ProfileImage = x.Member.ProfileImage                        
                    }
                }).ToList();
        }

        public ClubOfficerModel IsClubOfficer(int memberId, DateTime termDate)
        {
            var off = _context.ClubOfficers.SingleOrDefault(x => x.MemberId == memberId && x.TermEnd <= termDate && x.TermStart >= termDate);
            return
                off == null ? null : new ClubOfficerModel
                {
                    OfficerId = off.OfficerId,
                    Title = off.Title,
                    TermEnd = off.TermEnd,
                    TermStart = off.TermStart,
                    Member = new MemberModel
                    {
                        Email = off.Member.Email,
                        FirstName = off.Member.FirstName,
                        IsDisabled = off.Member.IsDisabled,
                        LastName = off.Member.LastName,
                        MemberCode = off.Member.MemberCode,
                        MemberId = off.MemberId,
                        MiddleName = off.Member.MiddleName,
                        Pin = off.Member.Pin,
                        ProfileImage = off.Member.ProfileImage
                    }
                };
        }

        public long PayDues(MemberDueModel model)
        {
            using (var trans = new CommittableTransaction(new TimeSpan(0, 5, 0)))
            {
                try
                {
                    var m = new MemberDue
                    {
                        AmountPaid = model.AmountPaid,
                        CreatedDate = DateTime.UtcNow.AddHours(-6),
                        DatePaid = DateTime.UtcNow.AddHours(-6),
                        MemberId = model.Member.MemberId
                    };
                    _context.MemberDues.Add(m);
                    _context.SaveChanges();
                    trans.Commit();
                    return
                        0;
                }
                catch (Exception ex)
                {
                    trans.Rollback(ex);
                    return -1;
                }
            }
        }

        public List<MemberModel> SearchMembers(MemberSearchModel search, out int total)
        {
            var query = _context.Members.AsQueryable();
            if(search.IsDisabled != null)
            {
                query = query.Where(x => x.IsDisabled == search.IsDisabled.Value);
            }
            if(!string.IsNullOrEmpty(search.LastName))
            {
                query = query.Where(x => x.LastName.ToLower().Contains(search.LastName.ToLower()));
            }
            if (!string.IsNullOrEmpty(search.FirstName))
            {
                query = query.Where(x => x.FirstName.ToLower().Contains(search.FirstName.ToLower()));
            }
            if (!string.IsNullOrEmpty(search.Email))
            {
                query = query.Where(x => x.Email.ToLower() == search.Email.ToLower());
            }
            if (!string.IsNullOrEmpty(search.MemberCode))
            {
                query = query.Where(x => x.MemberCode.ToLower() == search.MemberCode.ToLower());
            }
            if(search.MemberId != null)
            {
                query = query.Where(x => x.MemberId == search.MemberId.Value);
            }
            if(search.HasOptedOut != null)
            {
                query = query.Where(x => x.OptOut == search.HasOptedOut.Value);
            }
            total = query.Count();
            return
                query.OrderBy(x => x.LastName).ThenBy(x => x.FirstName).Skip(search.PageIndex * search.PageSize).Take(search.PageSize)
                    .ToList().Select(x => new MemberModel
                    {
                        Email = x.Email,
                        FirstName = x.FirstName,
                        IsDisabled = x.IsDisabled,
                        LastName = x.LastName,
                        MemberCode = x.MemberCode,
                        MemberId = x.MemberId,
                        MiddleName = x.MiddleName,
                        Pin = x.Pin,
                        ProfileImage = x.ProfileImage
                    }).ToList();
        }

        public MemberModel ValidateMember(string email, string pin)
        {
            var mem = _context.Members.SingleOrDefault(x => x.Pin == pin && x.Email.ToLower() == email.ToLower());
            return
                mem == null ? null :
                    new MemberModel
                    {
                        Email = mem.Email,
                        FirstName = mem.FirstName,
                        IsDisabled = mem.IsDisabled,
                        LastName = mem.LastName,
                        MemberCode = mem.MemberCode,
                        MemberId = mem.MemberId,
                        MiddleName = mem.MiddleName,
                        Pin = mem.Pin,
                        ProfileImage = mem.ProfileImage
                    };
        }
    }
}
