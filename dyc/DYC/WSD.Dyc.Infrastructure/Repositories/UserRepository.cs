﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WSD.Dyc.Domain.Entities;
using WSD.Dyc.Domain.Models;
using WSD.Dyc.Domain.Repositories;

namespace WSD.Dyc.Infrastructure.Repositories
{
    public class UserRepository : IUserRepository
    {
        private diverseyyachtclubEntities _context;
        public UserRepository(diverseyyachtclubEntities context)
        {
            _context = context;
        }

        public int AddUpdateUser(UserModel u)
        {
            var u1 = _context.Users.SingleOrDefault(x => x.Email.ToLower() == u.Email.ToLower());
            if(u1 != null)
            {
                u1.Pin = u.Pin;
                _context.SaveChanges();
                return
                    u1.UserId;
            }
            u1 = new User
            {
                Email = u.Email.ToLower(),
                Pin = u.Pin
            };
            _context.Users.Add(u1);
            _context.SaveChanges();
            return
                u1.UserId;
        }

        public List<UserModel> GetAllUsers()
        {
            return
               _context.Users.ToList().Select(x => new UserModel
               {
                   Email = x.Email,
                   Pin = x.Pin,
                   UserId = x.UserId
               }).ToList();
        }

        public void RemoveUser(int userId)
        {
            var user = _context.Users.SingleOrDefault(x => x.UserId == userId);
            if(user != null)
            {
                _context.Users.Remove(user);
                _context.SaveChanges();
            }
        }

        public UserModel ValidateUser(string email, string pin)
        {
            var user = _context.Users.SingleOrDefault(x => x.Email.ToLower() == email.ToLower() && x.Pin == pin);
            return
                user == null ? null : new UserModel
                {
                    UserId = user.UserId,
                    Pin = user.Pin,
                    Email = user.Email
                };
        }
    }
}
