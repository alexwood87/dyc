﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WSD.Dyc.Domain.Entities;
using WSD.Dyc.Domain.Models;
using WSD.Dyc.Domain.Repositories;

namespace WSD.Dyc.Infrastructure.Repositories
{
    public class NotesAndNewsRepository : INotesAndNewsRepository
    {
        private diverseyyachtclubEntities _context;
        public NotesAndNewsRepository(diverseyyachtclubEntities context)
        {
            _context = context;
        }
        public int AddUpdateCommodoreNote(CommodoreNoteModel note)
        {
            if(note.NoteId > 0)
            {
                var noteUp = _context.CommodoreNotes.Single(x => x.NoteId == note.NoteId);
                noteUp.Note = note.Note;
                _context.SaveChanges();
                return
                    note.NoteId;
            }
            var noteAdd = new CommodoreNote
            {
                Note = note.Note,
                CreatedDate = note.CreatedDate
            };
            _context.CommodoreNotes.Add(noteAdd);
            _context.SaveChanges();
            return
                noteAdd.NoteId;
        }

        public int AddUpdateNewsLetter(NewsLetterModel model)
        {
            if(model.NewsLetterId > 0)
            {
                var m = _context.NewsLetters.Single(x => x.LetterId == model.NewsLetterId);
                m.Title = model.Title;
                m.CreatedDate = model.CreatedDate;
                m.Content = model.Content;
                _context.SaveChanges();
                return
                    model.NewsLetterId;
            }
            var m2 = new NewsLetter
            {
                Content = model.Content,
                CreatedDate = model.CreatedDate,
                Title = model.Title
            };
            _context.NewsLetters.Add(m2);
            _context.SaveChanges();
            return
                m2.LetterId;
        }

        public List<CommodoreNoteModel> SearchCommodoreNotes(int? id, DateTime? currentDate, int pageIndex, int pageSize)
        {
            var query = _context.CommodoreNotes.AsQueryable();
            if (currentDate != null)
            {
                query = query.Where(c => c.CreatedDate >= currentDate.Value);
            }
            if(id != null)
            {
                query = query.Where(x => x.NoteId == id.Value);
            }
            return
                query.OrderBy(x => x.CreatedDate).Skip(pageIndex * pageSize).Take(pageSize)
                .ToList().Select(x => new CommodoreNoteModel
                {
                    CreatedDate = x.CreatedDate,
                    NoteId = x.NoteId,
                    Note = x.Note 
                }).ToList();

        }

        public List<NewsLetterModel> SearchNewsLetters(int? id, DateTime? currentDate, int pageIndex, int pageSize)
        {
            var query = _context.NewsLetters.AsQueryable();
            if (currentDate != null)
            {
                query = query.Where(c => c.CreatedDate >= currentDate.Value);
            }
            if(id != null)
            {
                query = query.Where(x => x.LetterId == id.Value);
            }
            return
                query.OrderBy(x => x.CreatedDate).Skip(pageIndex * pageSize).Take(pageSize)
                .ToList().Select(x => new NewsLetterModel
                {
                    CreatedDate = x.CreatedDate.GetValueOrDefault(DateTime.UtcNow.AddHours(-6)),
                    Content = x.Content,
                    NewsLetterId = x.LetterId,
                    Title = x.Title
                }).ToList();
        }
    }
}
