﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WSD.Dyc.Domain.Entities;
using WSD.Dyc.Domain.Models;
using WSD.Dyc.Domain.Repositories;

namespace WSD.Dyc.Infrastructure.Repositories
{
    public class ImageRepository : IImageRepository
    {
        diverseyyachtclubEntities _context;
        public ImageRepository(diverseyyachtclubEntities context)
        {
            _context = context;
        }
        public int AddOrUpdateImage(ImageModel model)
        {
            if(model.ImageId > 0)
            {
                var img = _context.MemberImages.Single(x => x.ImageId == model.ImageId);
                img.MemberImage1 = model.ImageContent;
                img.MemberId = model.Member.MemberId;
                _context.SaveChanges();
                return
                    model.ImageId;
            }
            var img2 = new MemberImage
            {
                MemberId = model.Member.MemberId,
                MemberImage1 = model.ImageContent
            };
            _context.MemberImages.Add(img2);
            _context.SaveChanges();
            return
                img2.ImageId;
        }

        public List<ImageModel> GetAllImages(int pageIndex, int pageSize, out int total)
        {
            total = _context.MemberImages.Count();
            return
                _context.MemberImages.OrderByDescending(x => x.ImageId).Skip(pageIndex * pageSize)
                .Take(pageSize).ToList().Select(x => new ImageModel
                {
                    ImageId = x.ImageId,
                    ImageContent = x.MemberImage1,
                    Member = new MemberModel
                    {
                        MemberId = x.MemberId,
                        Email  = x.Member.Email,
                        FirstName = x.Member.FirstName,
                        IsDisabled = x.Member.IsDisabled,
                        LastName = x.Member.LastName,
                        MemberCode = x.Member.MemberCode,
                        MiddleName = x.Member.MiddleName,
                        ProfileImage = x.Member.ProfileImage
                    }
                }).ToList();
        }
    }
}
