﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WSD.Dyc.Domain.Entities;
using WSD.Dyc.Domain.Repositories;
using WSD.Dyc.Domain.UnitsOfWork;
using WSD.Dyc.Infrastructure.Repositories;

namespace WSD.Dyc.Infrastructure.UnitsOfWork
{
    public class UnitOfWork : IUnitOfWork
    {
        private diverseyyachtclubEntities _context;
        private IEventRepository _eventRepository;
        private IImageRepository _imageRepository;
        private IMemberRepository _memberRepository;
        private INotesAndNewsRepository _notesAndNewsRepository;
        private IUserRepository _userRepository;
        private IShoppingRepository _shoppingRepository;
        public UnitOfWork(string connString)
        {
            _context = new diverseyyachtclubEntities(connString);
        }
        public IEventRepository EventRepository
        {
            get
            {
                return
                    _eventRepository ?? (_eventRepository = new EventRepository(_context));
            }

            set
            {
                _eventRepository = value;
            }
        }

        public IImageRepository ImageRepository
        {
            get
            {
                return
                    _imageRepository ?? (_imageRepository = new ImageRepository(_context));
            }

            set
            {
                _imageRepository = value;
            }
        }

        public IMemberRepository MemberRepository
        {
            get
            {
                return
                    _memberRepository ?? (_memberRepository = new MemberRepository(_context));
            }

            set
            {
                _memberRepository = value;
            }
        }

        public INotesAndNewsRepository NotesAndNewsRepository
        {
            get
            {
                return
                    _notesAndNewsRepository ?? (_notesAndNewsRepository = new NotesAndNewsRepository(_context));
            }

            set
            {
                _notesAndNewsRepository = value;
            }
        }

        public IShoppingRepository ShoppingRepository
        {
            get
            {
                return
                    _shoppingRepository ?? (_shoppingRepository = new ShoppingRepository(_context));
            }

            set
            {
                _shoppingRepository = value;
            }
        }

        public IUserRepository UserRepository
        {
            get
            {
                return
                    _userRepository ?? (_userRepository = new UserRepository(_context));
            }

            set
            {
                _userRepository = value;
            }
        }
    }
}
