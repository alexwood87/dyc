﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WSD.Dyc.BusinessLogic;

namespace WSD.Dyc.Windows.UserApp
{
    public partial class AppUserModifier : Form
    {
        public AppUserModifier()
        {
            InitializeComponent();
        }

        private void btnAddUser_Click(object sender, EventArgs e)
        {
            try
            {
                var logic = new AppBusinessLogic(ConfigurationManager.ConnectionStrings["ConnString"].ConnectionString);

                var u = new Domain.Models.UserModel
                {
                    Email = txtEmail.Text,
                    Pin = txtPin.Text
                };
                logic.AddUpdateUser(u);
                MessageBox.Show("User Added/Updated");
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
    }
}
