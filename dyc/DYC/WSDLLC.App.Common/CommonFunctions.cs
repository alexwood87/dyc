﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Data;
using System.Data.OleDb;
using System.IO;
using System.Net;
using System.Xml;
using System.Drawing;
namespace WSDLLC.App.Common
{
    public class CommonFunctions
    {

        public static byte[] PaintImageOnImage(byte[] pic, byte[] overlay, int x, int y, int width, int height)
        {
            var memoryPicStream = new MemoryStream(pic);
            var memoryWaterMark = new MemoryStream(overlay);
            var resultStream = new MemoryStream();
            using (Image image = Image.FromStream(memoryPicStream))//.FromFile(@"C:\Users\Public\Pictures\Sample Pictures\Desert.jpg"))
            using (Image watermarkImage = Image.FromStream(memoryWaterMark))//.FromFile(@"C:\Users\Public\Pictures\Sample Pictures\watermark.png"))
            using (Graphics imageGraphics = Graphics.FromImage(image))
            using (TextureBrush watermarkBrush = new TextureBrush(watermarkImage))
            {
                watermarkBrush.TranslateTransform(x, y);
                imageGraphics.FillRectangle(watermarkBrush, new Rectangle(new Point(x, y), new Size(watermarkImage.Width + 1, watermarkImage.Height)));
                image.Save(resultStream, System.Drawing.Imaging.ImageFormat.Jpeg);
            }
            return resultStream.ToArray();
        }

        /*
         <?xml version=\"1.0\" encoding=\"UTF-8\"?>\n
         <GeocodeResponse>\n 
         <status>OK</status>\n <result>\n  
         <type>premise</type>\n  
         <formatted_address>935 Oakland Ln, Aurora, IL 60504, USA</formatted_address>\n  
         <address_component>\n   
            <long_name>935</long_name>\n   
            <short_name>935</short_name>\n   
            <type>street_number</type>\n  
         </address_component>\n  
         <address_component>\n   
            <long_name>Oakland Lane</long_name>\n   
            <short_name>Oakland Ln</short_name>\n   
            <type>route</type>\n  
         </address_component>\n  
         <address_component>\n   
            <long_name>Aurora</long_name>\n   
            <short_name>Aurora</short_name>\n   
            <type>locality</type>\n   
            <type>political</type>\n  
         </address_component>\n  
         <address_component>\n   
            <long_name>Naperville Township</long_name>\n   
            <short_name>Naperville Township</short_name>\n   
            <type>administrative_area_level_3</type>\n   
            <type>political</type>\n  
         </address_component>\n  
         <address_component>\n   
            <long_name>DuPage County</long_name>\n   
            <short_name>Dupage County</short_name>\n   
            <type>administrative_area_level_2</type>\n   
            <type>political</type>\n  
         </address_component>\n  
         <address_component>\n   
            <long_name>Illinois</long_name>\n   
            <short_name>IL</short_name>\n   
            <type>administrative_area_level_1</type>\n   
            <type>political</type>\n  
         </address_component>\n  
         <address_component>\n   
            <long_name>United States</long_name>\n   
            <short_name>US</short_name>\n   
            <type>country</type>\n   
            <type>political</type>\n  
         </address_component>\n  
         <address_component>\n   
            <long_name>60504</long_name>\n   
            <short_name>60504</short_name>\n   
            <type>postal_code</type>\n  
         </address_component>\n  
         <address_component>\n   
            <long_name>5936</long_name>\n   
            <short_name>5936</short_name>\n   
            <type>postal_code_suffix</type>\n  
         </address_component>\n  
         <geometry>\n   
            <location>\n    
                <lat>41.7385682</lat>\n    
                <lng>-88.2436444</lng>\n   
            </location>\n   
            <location_type>ROOFTOP</location_type>\n   <viewport>\n    <southwest>\n     <lat>41.7372192</lat>\n     <lng>-88.2449934</lng>\n    </southwest>\n    <northeast>\n     <lat>41.7399172</lat>\n     <lng>-88.2422955</lng>\n    </northeast>\n   </viewport>\n   <bounds>\n    <southwest>\n     <lat>41.7384859</lat>\n     <lng>-88.2437439</lng>\n    </southwest>\n    <northeast>\n     <lat>41.7386505</lat>\n     <lng>-88.2435450</lng>\n    </northeast>\n   </bounds>\n  </geometry>\n  <partial_match>true</partial_match>\n  <place_id>ChIJO8jiX1L3DogRuBwOu0jn_3s</place_id>\n </result>\n</GeocodeResponse>\n"
             */
        public static void GeoCode(string address, string address2,
            string city, string state, string postalcode, string country,
            string geoCodeKey, out decimal latitude, out decimal longitude)
        {
            //https://maps.googleapis.com/maps/api/geocode/outputFormat?
            HttpWebRequest req = (HttpWebRequest)WebRequest.Create("https://maps.googleapis.com/maps/api/geocode/xml?address="+address + " " + (address2 ?? "") +
                city + ", " + (state ?? " ") + (postalcode ?? " ") + (country ?? " ") +"&key=" +geoCodeKey);
            var resp = req.GetResponse();
            var res = new StreamReader(resp.GetResponseStream());
            var xml = res.ReadToEnd();
            var xdoc = new XmlDocument();
            xdoc.LoadXml(xml);
            var pathLat = xdoc.SelectSingleNode("/GeocodeResponse/result/geometry/location/lat");
            var pathLng = xdoc.SelectSingleNode("/GeocodeResponse/result/geometry/location/lng");
            decimal d, d2;
            if (decimal.TryParse(pathLat.InnerText, out d) && decimal.TryParse(pathLng.InnerText, out d2))
            {
                longitude = d2;
                latitude = d;
                return;
            } 
            throw new Exception("Failed to Geo Code!");
        }
        public static DataSet ParseExcelFile(string path, string[] sheetNames)
        {
            using (OleDbConnection conn = new OleDbConnection())
            {
                var ds = new DataSet();
                foreach (var sheetName in sheetNames)
                {
                    DataTable dt = new DataTable(sheetName);
                    string Import_FileName = path;
                    string fileExtension = Path.GetExtension(Import_FileName);
                    if (fileExtension == ".xls")
                        conn.ConnectionString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + Import_FileName + ";" + "Extended Properties='Excel 8.0;HDR=YES;'";
                    if (fileExtension == ".xlsx")
                        conn.ConnectionString = "Provider=Microsoft.Ace.OLEDB.12.0;Data Source=" + Import_FileName + ";" + "Extended Properties='Excel 16.0 Xml;HDR=YES;'";
                    using (OleDbCommand comm = new OleDbCommand())
                    {
                        comm.CommandText = "Select * from [" + sheetName + "]";

                        comm.Connection = conn;

                        using (OleDbDataAdapter da = new OleDbDataAdapter())
                        {
                            da.SelectCommand = comm;
                            da.Fill(dt);
                            ds.Tables.Add(dt);
                        }

                    }
                }
                return ds;
            }

        }
        public static void Log(string type, string logpathFile, string message)
        {
            var m = string.Format("{0}: {1}", type, message);
            if (!Directory.Exists(logpathFile))
            {
                Directory.CreateDirectory(logpathFile);
            }

            File.WriteAllText(logpathFile + "\\log_" + DateTime.UtcNow.ToFileTime() + ".txt", message);
        }
        public static void SendEmail(string from, string to, string message, string title, string username, string password)
        {
            var client = new System.Net.Mail.SmtpClient();//add custom port here
                                                          //This object stores the authentication values

            client.UseDefaultCredentials = false;
            client.Credentials = new NetworkCredential(username, password);
            client.Host = "smtp.office365.com";
            client.EnableSsl = true;
            client.TargetName = "STARTTLS/smtp.office365.com";
            client.Port = 587;



            System.Net.Mail.MailMessage messageM = new System.Net.Mail.MailMessage(from, to, title, message);
            messageM.IsBodyHtml = true;
            client.Send(messageM);
        }
        public static int SendSMS(string AccountID, string Email, string Password, string Recipient, string Message, string providerUrl)
        {
            WebClient Client = new WebClient();
            string RequestURL, RequestData;

            RequestURL = providerUrl;

            RequestData = "AccountId=" + AccountID
                + "&Email=" + System.Web.HttpUtility.UrlEncode(Email)
                + "&Password=" + System.Web.HttpUtility.UrlEncode(Password)
                + "&Recipient=" + System.Web.HttpUtility.UrlEncode(Recipient)
                + "&Message=" + System.Web.HttpUtility.UrlEncode(Message);

            byte[] PostData = Encoding.ASCII.GetBytes(RequestData);
            byte[] Response = Client.UploadData(RequestURL, PostData);

            string Result = Encoding.ASCII.GetString(Response);
            int ResultCode = Convert.ToInt32(Result.Substring(0, 4));

            return ResultCode;
        }


    }
}
