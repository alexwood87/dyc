﻿function toReadDataUrl(canvasId)
{
    var el = document.getElementById(canvasId);
    return
        el.toDataURL('image/png', 1.0);
}

function ImageToCanvas(imgId, canvasId)
{
    var c = document.getElementById(canvasId);
    var ctx = c.getContext("2d");
    var img = document.getElementById(imgId);
    ctx.drawImage(img, 0, 0);
}
function supportsToDataURL() {
    var c = document.createElement("canvas");
    var data = c.toDataURL("image/png");
    return (data.indexOf("data:image/png") == 0);
}
function MakeBlackAndWhiteCanvas(canvas, x, y, width, height) {
    //var canvas = document.getElementById(canvasId);
    if (!supportsToDataURL())
        alert('browser not supported');
    var context = canvas.getContext("2d");

    var imgd = context.getImageData(x, y, width, height);

    var pix = imgd.data;

    for (var i = 0, n = pix.length; i < n; i += 4) {
        var grayscale = pix[i] * .3 + pix[i + 1] * .59 + pix[i + 2] * .11;
        pix[i] = grayscale;   // red
        pix[i + 1] = grayscale;   // green
        pix[i + 2] = grayscale;   // blue
        // alpha
    }

    context.putImageData(imgd, 0, 0);

    var dataUrl = canvas.toDataURL("image/png");

    return dataUrl;
}
function RemoveRedEyeCanvas(canvas, x, y, width, height) {
    //var canvas = document.getElementById(canvasId);
    if (!supportsToDataURL())
        alert('browser not supported');
    var context = canvas.getContext("2d");

    var imgd = context.getImageData(x, y, width, height);

    var pixel = imgd.data;

    for (var i = 0, n = pixel.length; i < n; i += 4) {
        var R = pixel[i];
        var G = pixel[i + 1];
        var B = pixel[i + 2];
        //var alpha = pixel[3];

        var redIntensity = (R / ((B + G) / 2.0));

        if (redIntensity > 1.5) {
            pixel[i] = (G + B) / 2.0;
        }
        // alpha
    }

    context.putImageData(imgd, 0, 0);

    var dataUrl = canvas.toDataURL("image/png");

    return dataUrl;
}

function MakeSepiaCanvas(canvas, x, y, width, height) {
    //var canvas = document.getElementById(canvasId);
    if (!supportsToDataURL())
        alert('browser not supported');
    var context = canvas.getContext("2d");

    var imgd = context.getImageData(x, y, width, height);

    var pixel = imgd.data;

    for (var i = 0, n = pixel.length; i < n; i += 4) {
        var R = pixel[i];
        var G = pixel[i + 1];
        var B = pixel[i + 2];
        pixel[i] = R * 0.393 + G * 0.769 + B * 0.189;
        pixel[i + 1] = R * 0.349 + G * 0.686 + B * 0.168;
        pixel[i + 2] = R * .272 + G * 0.534 + B * 0.131;
        // alpha
    }

    context.putImageData(imgd, 0, 0);

    var dataUrl = canvas.toDataURL("image/png");

    return dataUrl;
}
function AddFileUploadCameraIOS(imgContainerId)
{
    uploads++;
    $('#' + imgContainerId).append('<div><input type="file" name="cameraupload_' + uploads + '" id="cameraupload_'+uploads+'" accept="images/*" /> <a href="#" onclick="RemoveFileUpload(this,null);">Remove</a></div>');
}

function ReadDataURL(canvasId, fileIndex)
{
    var canvas = document.getElementById(canvasId);
    var context = canvas.getContext("2d");
    
    if (this.files && this.file[fileIndex]) {
        var FR = new FileReader();
        FR.onload = function (e) {
            var img = new Image();
            img.onload = function () {
                context.drawImage(img, 0, 0);
            };
            img.src = e.target.result;
        };
        FR.readAsDataURL(this.files[fileIndex]);
    }
    
}
function AjaxUploadOfImage(dataUrl)
{
    $.ajax({
        url: '/Members/PostImage',
        method: 'POST',
        data: {
            img64: dataUrl
        },
        success: function (data) {
            if (data.Success) {
                alert('Image Uploaded!');
            }
        },
        error: function (a, b, c) {
            alert(c);
        }
    });
}
$(document).ready(function () {
    if ($('#fileuploader')) {
        $('#fileuploader').change(function () {
            ReadDataURL('imgCanvas', 0);
            var dataUrl = toReadDataUrl('imgCanvas');
    
            AjaxUploadOfImage(dataUrl);
        });
    }
    if($('#fuImg'))
    {
        $('#fuImg').change(function () {
            ReadDataURL('preview', 0);

        });
    }
   
});
function StopRecordingImages(videoId)
{
    document.getElementById(videoId).src = null;
}

var uploads = 0;
function AddFileUploadLibrary()
{
    uploads++;
    $('#imgContainer').append('<div><input type="file" accept="images/*" id="fu_'+uploads+'" name="fu_'+uploads+'" capture="camera"/><a href="#" onclick="RemoveFileUpload(this, null);">Remove</a></div>');
}
var mediaRecorder;
function AddFileUploadCamera(videoId)
{
    LoadCamera(videoId);
    uploads++;
    $('#imgContainer').append('<div> <input id="cameraUpload_' + uploads + '" type="hidden" name="cameraupload_' + uploads + '" /></div>');
    var id = "cameraUpload_" + uploads;
    
}
function TakePicture(videoId, imgContainerId, canvasId)
{
    StopRecording();
   
}
function RemoveFileUpload(element, videoId)
{
    if (videoId)
    {
        var vid = document.getElementById(videoId);
        vid.pause();
        vid.src = null;
    }
    $(element).parent().remove();
}