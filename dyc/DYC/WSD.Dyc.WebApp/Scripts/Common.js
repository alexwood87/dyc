﻿function getURLParameter(name) {
    return decodeURIComponent((new RegExp('[?|&]' + name + '=' + '([^&;]+?)(&|#|;|$)').exec(location.search) || [null, ''])[1].replace(/\+/g, '%20')) || null;
}
function getBrowser() {
    if (navigator.userAgent.toLowerCase().indexOf('firefox') !== -1) {
        return "Firefox"

    }
    if (navigator.userAgent.toLowerCase().indexOf('android') !== -1) {
        return "Android";
    }
    if (navigator.userAgent.toLowerCase().indexOf('chrome') !== -1) {
        return "Chrome";
    }
    if (navigator.userAgent.toLowerCase().indexOf('safari') !== -1) {
        return "Safari";
    }
    
    if (navigator.userAgent.toLowerCase().indexOf('ios') !== -1)
    {
        return "IOS";
    }
    if (navigator.userAgent.toLocaleLowerCase().indexOf('mac') !== -1)
    {
        return "IOS";
    }
    return "Other";
}
$(document).ready(function () {
    var now = new Date();
    $('.datepicker').datepicker();
    $('.datetimepicker').datetimepicker({
        startDate: now.getFullYear() + "/" + (now.getMonth()+1) + "/" + now.getDate()
    });
    $('.numbersonly').blur(function () {
        var i = parseFloat($(this).val());
        if(isNaN(i))
        {
            alert('It Must Be A Number!');
        }
    });
    $('.intsonly').blur(function () {
        var i = parseInt($(this).val());
        if(isNaN(i))
        {
            alert('Data Must Be A Whole Number!');
        }
    });
    $('.dialogwithclose').hide();
    $('.dialogwithclose').dialog({
        close: function (event, ui) {
            window.location.reload(true);
        },
        autoOpen: false
    });
    $('.dialogwithoutclose').hide();
    $('.dialogwithoutclose').dialog({
        autoOpen: false
    });
    $('.filtertable').filterTable({
        search: 'FilterOnAny'
    });
    $('.sortabletable').sortable();
    $('.cbxeventenddate').change(function () {
        if($(this).is(':checked'))
        {
            $('.eventenddate').show();
        }
        else
        {
            $('.eventenddate').hide();
        }
    });
});