﻿/*
http://graph.facebook.com/userid?limit=20&fields=events.fields(description,end_time,location,name,ticket_uri,venue)&access_token=access_token
*/
/**
		 * This is the getPhoto library
		 */
function makeFacebookPhotoURL(id, accessToken) {
    return 'https://graph.facebook.com/' + id + '/picture?access_token=' + accessToken;
}
function login(callback) {
    FB.login(function (response) {
        if (response.authResponse) {
            //console.log('Welcome!  Fetching your information.... ');
            if (callback) {
                callback(response);
            }
        } else {
            console.log('User cancelled login or did not fully authorize.');
        }
    }, { scope: 'user_photos' });
}
function getAlbums(callback) {
    FB.api(
            '/me/albums',
            { fields: 'id,cover_photo' },
            function (albumResponse) {
                //console.log( ' got albums ' );
                if (callback) {
                    callback(albumResponse);
                }
            }
        );
}
function getPhotosForAlbumId(albumId, callback) {
    FB.api(
            '/' + albumId + '/photos',
            { fields: 'id' },
            function (albumPhotosResponse) {
                //console.log( ' got photos for album ' + albumId );
                if (callback) {
                    callback(albumId, albumPhotosResponse);
                }
            }
        );
}
function getLikesForPhotoId(photoId, callback) {
    FB.api(
            '/' + albumId + '/photos/' + photoId + '/likes',
            {},
            function (photoLikesResponse) {
                if (callback) {
                    callback(photoId, photoLikesResponse);
                }
            }
        );
}
function getPhotos(callback) {
    var allPhotos = [];
    var accessToken = '';
    login(function (loginResponse) {
        accessToken = loginResponse.authResponse.accessToken || '';
        getAlbums(function (albumResponse) {
            var i, album, deferreds = {}, listOfDeferreds = [];
            for (i = 0; i < albumResponse.data.length; i++) {
                album = albumResponse.data[i];
                deferreds[album.id] = $.Deferred();
                listOfDeferreds.push(deferreds[album.id]);
                getPhotosForAlbumId(album.id, function (albumId, albumPhotosResponse) {
                    var i, facebookPhoto;
                    for (i = 0; i < albumPhotosResponse.data.length; i++) {
                        facebookPhoto = albumPhotosResponse.data[i];
                        allPhotos.push({
                            'id': facebookPhoto.id,
                            'added': facebookPhoto.created_time,
                            'url': makeFacebookPhotoURL(facebookPhoto.id, accessToken)
                        });
                    }
                    deferreds[albumId].resolve();
                });
            }
            $.when.apply($, listOfDeferreds).then(function () {
                if (callback) {
                    callback(allPhotos);
                }
            }, function (error) {
                if (callback) {
                    callback(allPhotos, error);
                }
            });
        });
    });
}
/**
		 * This is the bootstrap / app script
		 */
// wait for DOM and facebook auth
var docReady = $.Deferred();
var facebookReady = $.Deferred();
$(document).ready(docReady.resolve);
window.fbAsyncInit = function () {
    FB.init({
        appId: '138804179958048',//'317880141941062',//'1870876446491126',
        channelUrl: '//localhost/channel.html',
        status: true,
        cookie: true,
        xfbml: true
    });
    facebookReady.resolve();
};
$.when(docReady, facebookReady).then(function () {
    if (typeof getPhotos !== 'undefined') {
        //getPhotos(function (photos) {
        //    console.log(photos);
        //});
    }
    $('#btnFacebookImport').click(function () {
        Login();
    });
});
// call facebook script
(function (d) {
    var js, id = 'facebook-jssdk'; if (d.getElementById(id)) { return; }
    js = d.createElement('script'); js.id = id; js.async = true;
    js.src = "http://connect.facebook.net/en_US/all.js";
    d.getElementsByTagName('head')[0].appendChild(js);
}(document));
$(document).ready(function () {
    $('.facebookimg').click(function () {
        if($(this).hasClass('facebookactive'))
        {
            $(this).removeClass('facebookactive');
        }
        else
        {
            $(this).addClass('facebookactive');
        }
    });
    $('#facebookUpload').click(function () {
        $('.facebookactive').each(function (i, element) {
            var url = $(element).attr('src');
            UploadMyPicture(url);
        });
    });
});
function UploadMyPicture(picUrl)
{
    $.ajax({
        url: '/Members/FacebookProfilePicUpload',
        method: 'POST',
        data: {
            imgUrl: picUrl
        },
        success: function (data) {
            if (data && data.Success) {
                console.log("Facebook Image Imported");
                //alert('Facebook Profile Picture Imported');
            }
        }
    });
}

function Login() {
    FB.login(function (response) {
        if (response.authResponse) {
            
            // some code here
            FB.api('/me', function (response2) {
                FB.api('/me/picture?type=normal', function (response3) {
                    $("#facebookimages").append('<li><img src="'+ response3.data.url + '" alt="Profile Image" class="facebookimg"/></li>');
                    //alert(response3.data.url);
                    //UploadMyPicture(response3.data.url);
                });
                getPhotos(function (photos, error) {
                    if(error)
                    {
                        alert(error);
                        return;
                    }
                    if(photos)
                    {
                        for(var pi in photos)
                        {
                            //alert(photos[pi]);
                            var d = photos[pi];
                            //UploadMyPicture(d.url);
                            $("#facebookimages").append('<li><img src="' + d.url + '" alt="Profile Image" class="facebookimg"/></li>');

                        }
                        $('#loader').hide();
                    }
                });
                FB.api('/me/events?fields=description,can_guests_invite,attending_count,category,cover,declined_count,end_time,guest_list_enabled,id,interested_count,is_canceled,is_page_owned,is_viewer_admin,maybe_count,name,noreply_count,owner,parent_group,place,rsvp_status,start_time,ticket_uri,timezone,type,updated_time', function (response3) {
                    if(response3.data)
                    {
                        $('#loader').show();
                        for(var i in response3.data)
                        {
                            var d = response3.data[i];
                            var s = d.start_time;
                            var second = s % 60;
                            var minute = (s / 60) % 60;
                            var hour = (s / (60*60)) % 24
                            var day = (s / (60*60*24)) % 31
                            var month = (s / (60*60*24*31)) % 12;
                            var year = s / (60*60*24*31*12 );
                            var isOwner = d.owner.id == response2.data.id;
                            $.ajax({
                                url: '/EventApp/FacebookAddEvent',
                                method: 'POST',
                                data: {
                                    Description: d.description,
                                    Name: d.name,
                                    isOwner: isOwner,
                                    eventDate: month + '/' + day + '/' + year + ' ' + hour + ':'
                                    + minute + ':' + second
                                },
                                success: function (data) {
                                    
                                },
                                error : function(a,b,c)
                                {
                                    $('#loader').hide();
                                    alert(c);
                                }
                            });
                        }
                    }
                });
            });
    } else {
      alert("Login attempt failed!");
    }
}, { scope: 'email,user_photos,publish_actions, user_events' });

}


