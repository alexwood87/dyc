﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WSD.Dyc.BusinessLogic;
using WSD.Dyc.Core.Contracts;
using WSD.Dyc.Domain.Models;
using WSD.Dyc.WebApp.Helpers;

namespace WSD.Dyc.WebApp.Controllers
{
    [SecureContextHeader]
    [AuthorizationHeader]
    public class AdminController : Controller
    {
        private IAppBusinessLogic _logic;
        public AdminController()
        {
            _logic = new AppBusinessLogic(ConfigurationManager.ConnectionStrings["ConnString"].ConnectionString);
        }
        // GET: Admin
        [HttpGet]
        public ActionResult Users()
        {
            return View(_logic.GetAllUsers());
        }
        [HttpGet]
        public ActionResult Officers()
        {
            return
                View(_logic.GetOfficers());
        }
        [HttpGet]
        public ActionResult EditMember(int id)
        {
            int total = 0;
            return
                View(_logic.SearchMembers(new MemberSearchModel
                {
                    MemberId = id, 
                    PageSize = 1
                }, out total).Single());
        }
        [HttpPost]
        public ActionResult EditMember(MemberModel member)
        {
            try
            {
                _logic.AddOrUpdateMembers(member);
                return
                    RedirectToAction("Members");
            }
            catch(Exception ex)
            {
                ViewBag.Error = ex.Message;
                return
                    View(member);
            }
        }
        [HttpGet]
        public ActionResult Members()
        {
            int total;
            return
                View(_logic.SearchMembers(new MemberSearchModel
                {
                    PageSize = int.MaxValue
                }, out total));
        }
        [HttpGet]
        public ActionResult MembersList(int pageIndex, int pageSize)
        {
            try {
                int total = 0;
                var ms = _logic.SearchMembers(new Domain.Models.MemberSearchModel { PageIndex = pageIndex, PageSize = pageSize }, out total);
                ViewBag.Total = total;
                return
                    Json(new { Members = ms, Total = total, Success = true }, JsonRequestBehavior.AllowGet);
            }
            catch(Exception ex)
            {
                return
                    Json(new { Success = false, Error = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpGet]
        public ActionResult RemoveUser(int id)
        {
            try
            {
                _logic.RemoveUser(id);
                return
                     RedirectToAction("Users");
            }
            catch(Exception ex)
            {
                return
                    Json(new { Success = false, Error = ex.Message}, JsonRequestBehavior.AllowGet);
            }
        }
        
        [HttpPost]
        public ActionResult DisableMember(int id)
        {
            try
            {
                var total = 0;
                var m = _logic.SearchMembers(new MemberSearchModel
                {
                    PageSize = 1,
                    MemberId = id
                }, out total).Single();
                m.IsDisabled = true;
                _logic.AddOrUpdateMembers(m);
                return
                    Json(new { Success = true }, JsonRequestBehavior.AllowGet);
            }
            catch(Exception ex)
            {
                return
                    Json(new { Success = false, Error = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        public ActionResult EnableMember(int id)
        {
            try
            {
                var total = 0;
                var m = _logic.SearchMembers(new MemberSearchModel
                {
                    PageSize = 1,
                    MemberId = id
                }, out total).Single();
                m.IsDisabled = false;
                _logic.AddOrUpdateMembers(m);
                return
                    Json(new { Success = true }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return
                    Json(new { Success = false, Error = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpGet]
        public ActionResult AppointOfficer()
        {
            return
                View();
        }
        [HttpPost]
        public ActionResult AppointOfficer(int MemberId, ClubOfficerModel officer)
        {
            try
            {
                officer.Member = new MemberModel
                {
                    MemberId = MemberId
                };
                _logic.AddOrUpdateOfficer(officer);
                return
                    Redirect("~/Admin/Officers");
            }
            catch(Exception ex)
            {
                ViewBag.Error = ex.Message;
                return
                    View(officer);
            }
        }
        [HttpGet]
        public ActionResult MenuItems()
        {
            var total = 0;
            var items = _logic.GetAllMenuItemsAdmin(0, int.MaxValue, out total);
            ViewBag.Total = total;
            return
                View(items);
        }
        [HttpGet]
        public ActionResult EditMenuItem(int id)
        {
            var total = 0;
            return
                View(_logic.GetAllMenuItemsAdmin(0, int.MaxValue, out total).Single(x => x.MenuItemId == id));
        }
        [HttpPost]
        public ActionResult EditMenuItem(MenuItemModel item)
        {
            try
            {
                item.Picture = ReadPicture();
                _logic.AddUpdateMenuItem(item);
                return
                    RedirectToAction("MenuItems");
            }
            catch (ArgumentException ex)
            {
                ViewBag.Error = ex.Message;
                return
                    View(item);
            }
        }
        [HttpGet]
        public ActionResult AddMenuItem()
        {
            return
                View();
        }
        [HttpPost]
        public ActionResult AddMenuItem(MenuItemModel itemModel)
        {

            try
            {
                itemModel.Picture = ReadPicture();
                _logic.AddUpdateMenuItem(itemModel);
                return
                    RedirectToAction("MenuItems");
            }
            catch (ArgumentException ex)
            {
                ViewBag.Error = ex.Message;
                return
                    View(itemModel);
            }
        }
        [HttpGet]
        public ActionResult ShoppingItems()
        {
            var total = 0;
            var items = _logic.GetAllShopItemsAdmin(0, int.MaxValue, out total);
            ViewBag.Total = total;
            return
                View(items);
        }
        [HttpGet]
        public ActionResult Events()
        {
            return
                View(_logic.SearchEvents(new EventSearchModel {
                    PageSize = int.MaxValue,
                    FromDate = DateTime.Now.AddYears(-1)
                }));
        }
        [HttpGet]
        public ActionResult EditEvent(int id)
        {
            return
                 View(_logic.SearchEvents(new EventSearchModel
                 {
                     PageSize = 1,
                     EventId = id
                 }).Single());
        }
        [HttpGet]
        public ActionResult CreateEvent()
        {
            return
                View();
        }
        [HttpPost]
        public ActionResult CreateEvent(CalendarEventModel eventModel)
        {
            try
            {
                eventModel.EventId = 0;
                _logic.AddUpdateEvent(eventModel);
                return
                    RedirectToAction("Events");
            }
            catch (Exception ex)
            {
                ViewBag.Error = ex.Message;
                return
                    View(eventModel);
            }
        }
        [HttpPost]
        public ActionResult EditEvent(CalendarEventModel eventModel)
        {
            try
            {
                _logic.AddUpdateEvent(eventModel);
                return
                    RedirectToAction("Events");
            }
            catch (Exception ex)
            {
                ViewBag.Error = ex.Message;
                return
                    View(eventModel);
            }
        }
        [HttpGet]
        public ActionResult OnlineOrders()
        {
            return
                View(_logic.FindBarGrillOrders(null));
        }
        [HttpGet]
        public ActionResult ShopOrders()
        {
            return
                View(_logic.FindShoppingOrders(null));
        }
        [HttpGet]
        public ActionResult ShopOrderDetails(long id)
        {
            return
                View(_logic.SearchShoppingOrders(new ShoppingSearchModel
                {
                    PageSize = 1,
                    OrderId = id
                }).Single());
        }
        [HttpGet]
        public JsonResult MarkAsFulfilledOnlineOrder(long orderId)
        {
            try
            {
                var o = _logic.SearchOnlineOrders(new ShoppingSearchModel
                {
                    OrderId = orderId,
                    PageSize = 1
                }).Single();
                o.DateFulfilled = DateTime.UtcNow.AddHours(-6);
                _logic.AddOnlineOrder(o, null);
                return
                    Json(new { Success = true }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return
                    Json(new { Success = false, Error = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpGet]
        public JsonResult MarkAsFulfilledShoppingOrder(long orderId)
        {
            try
            {
                var o = _logic.SearchShoppingOrders(new ShoppingSearchModel
                {
                    OrderId = orderId,
                    PageSize = 1
                }).Single();
                o.DateFulfilled = DateTime.UtcNow.AddHours(-6);
                _logic.AddShoppingOrder(o, null);
                return
                    Json(new { Success = true }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return
                    Json(new { Success = false, Error = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpGet]
        public ActionResult OnlineOrderDetails(int id)
        {
            return
                View(_logic.SearchOnlineOrders(new ShoppingSearchModel
                {
                    PageSize = 1,
                    OrderId = id
                }).Single());
        }
        [HttpGet]
        public ActionResult EditShoppingItem(int id)
        {
            var total = 0;
            return
                View(_logic.GetAllShopItemsAdmin(0, int.MaxValue, out total).Single(x => x.ShopItemId == id));
        }
        private byte[] ReadPicture()
        {
            if (Request.Files.Count > 0)
            {
                var len = Request.Files[0].ContentLength;
                var reader = new System.IO.BinaryReader(Request.Files[0].InputStream);
                var buf = reader.ReadBytes(len);
                reader.Close();
                return
                    buf;
            }
            return
                null;
        }
        [HttpPost]
        public ActionResult EditShoppingItem(ShopItemModel item)
        {
            try
            {
                item.Picture = ReadPicture();
                _logic.AddUpdateShoppingItem(item);
                return
                    RedirectToAction("ShoppingItems");
            }
            catch (ArgumentException ex)
            {
                ViewBag.Error = ex.Message;
                return
                    View(item);
            }
        }
        [HttpGet]
        public ActionResult AddShoppingItem()
        {
            return
                View();
        }
        [HttpPost]
        public ActionResult AddShoppingItem(ShopItemModel item)
        {

            try
            {
                item.Picture = ReadPicture();
                _logic.AddUpdateShoppingItem(item);
                return
                    RedirectToAction("ShoppingItems");
            }
            catch (ArgumentException ex)
            {
                ViewBag.Error = ex.Message;
                return
                    View(item);
            }
        }
        [HttpGet]
        public ActionResult Index()
        {
            return
                View();
        }
        [HttpGet]
        public ActionResult NewsLetters()
        {
            return
                View(_logic.SearchNewsLetters(null, null, 0, int.MaxValue));
        }
        [HttpGet]
        public ActionResult EditNewsLetter(int id)
        {
            return
                View(_logic.SearchNewsLetters(id, null, 0, 1).Single());
        }
        [HttpPost]
        public ActionResult EditNewsLetter(NewsLetterModel letter)
        {
            try
            {
                _logic.AddUpdateNewsLetter(letter, ConfigurationManager.AppSettings["FromEmail"], ConfigurationManager.AppSettings["EmailUsername"], ConfigurationManager.AppSettings["EmailPassword"]);
                return
                    RedirectToAction("NewsLetters");
            }
            catch(Exception ex)
            {
                ViewBag.Error = ex.Message;
                return
                    View(letter);
            }
        }
        [HttpGet]
        public ActionResult AddNewsLetter()
        {
            return
                View();
        }
        [HttpPost]
        public ActionResult AddNewsLetter(NewsLetterModel newsletter)
        {
            try
            {
                _logic.AddUpdateNewsLetter(newsletter, ConfigurationManager.AppSettings["FromEmail"], ConfigurationManager.AppSettings["EmailUsername"], ConfigurationManager.AppSettings["EmailPassword"]);
                return
                    RedirectToAction("Index");
            }
            catch(Exception ex)
            {
                ViewBag.Error = ex.Message;
                return
                    View(newsletter);
            }
        }

        [HttpGet]
        public ActionResult CommodoreNotes()
        {
            return
                View(_logic.SearchCommodoreNotes(null, null, 0, int.MaxValue));
        }
        [HttpGet]
        public ActionResult EditCommodoreNote(int id)
        {
            return
                View(_logic.SearchCommodoreNotes(id, null, 0, 1).Single());
        }
        [HttpPost]
        public ActionResult EditCommodoreNote(CommodoreNoteModel noteModel)
        {
            try
            {
                _logic.AddUpdateCommodoreNote(noteModel, ConfigurationManager.AppSettings["FromEmail"], ConfigurationManager.AppSettings["EmailUsername"], ConfigurationManager.AppSettings["EmailPassword"]);
                return
                    RedirectToAction("CommodoreNotes");
            }
            catch (Exception ex)
            {
                ViewBag.Error = ex.Message;
                return
                    View(noteModel);
            }
        }
        [HttpGet]
        public ActionResult PostDocuments()
        {
            return
                View();
        }
        [HttpPost]
        public ActionResult PostPdfDocuments()
        {
            if (Request.Files.Count > 0)
            {
                var key = Request.Files.AllKeys.SingleOrDefault(x => x == "Menu");
                var key2 = Request.Files.AllKeys.SingleOrDefault(x => x == "MembershipForm");
                if (key2 != null && Request.Files[key2] != null)
                {
                    Request.Files[key2].SaveAs("~/Documents/MembershipForm.pdf");
                }
                if(key != null && Request.Files[key] != null)
                {
                    Request.Files[key].SaveAs("~/Documents/Menu.pdf");
                }
                var key3 = Request.Files.AllKeys.SingleOrDefault(x => x == "BreakfastMenu");
                if(key3 != null && Request.Files[key3] != null)
                {
                    Request.Files[key3].SaveAs("~/Documents/BreakfastMenu.pdf");
                }
            }
            return
                RedirectToAction("Index");
        }
        [HttpGet]
        public ActionResult PostCommodoreNote()
        {
            return
                View();
        }

        [HttpPost]
        public ActionResult PostCommodoreNote(CommodoreNoteModel noteModel)
        {
            try
            {
                _logic.AddUpdateCommodoreNote(noteModel, ConfigurationManager.AppSettings["FromEmail"], ConfigurationManager.AppSettings["EmailUsername"], ConfigurationManager.AppSettings["EmailPassword"]);
                return
                    RedirectToAction("CommodoreNotes");
            }
            catch (Exception ex)
            {
                ViewBag.Error = ex.Message;
                return
                    View(noteModel);
            }
        }

        [HttpPost]
        public ActionResult AddCalendarEvent(CalendarEventModel ev)
        {
            try
            {
                _logic.AddUpdateEvent(ev);
                return
                    Redirect("Index");
            }
            catch(Exception ex)
            {
                ViewBag.Error = ex.Message;
                return
                    View(ev);
            }
        }
        
        [HttpGet]
        public ActionResult EditOfficer(int id)
        {
            return
                View(_logic.GetOfficers().Single(x => x.OfficerId == id));
        }

        [HttpPost]
        public ActionResult EditOfficer(int MemberId, ClubOfficerModel officer)
        {
            try
            {
                officer.Member = new MemberModel
                {
                    MemberId = MemberId
                };
                _logic.AddOrUpdateOfficer(officer);
                return
                    RedirectToAction("Officers");
            }
            catch(Exception ex)
            {
                ViewBag.Error = ex.Message;
                return
                    View(officer);
            }
        }
    }
}