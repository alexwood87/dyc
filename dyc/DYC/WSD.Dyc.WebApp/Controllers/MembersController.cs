﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using WSD.Dyc.BusinessLogic;
using WSD.Dyc.Core.Contracts;
using WSD.Dyc.Domain.Models;
using WSD.Dyc.WebApp.Helpers;

namespace WSD.Dyc.WebApp.Controllers
{
    [SecureContextHeader]
    [AuthorizationHeader]
    public class MembersController : Controller
    {
        private IAppBusinessLogic _logic;
        public MembersController()
        {
            _logic = new AppBusinessLogic(ConfigurationManager.ConnectionStrings["ConnString"].ConnectionString);
        }

        // GET: Members
        [HttpGet]
        public ActionResult Dashboard()
        {
            return View();
        }
        [HttpGet]
        public ActionResult MyProfile()
        {
            return
                View((MemberModel)Session["Member"]);
        }
        [HttpPost]
        public ActionResult MyProfile(MemberModel member)
        {
            try
            {
                var m = ((MemberModel)Session["Member"]);
                member.MemberId = m.MemberId;
                member.IsDisabled = m.IsDisabled;
                member.MemberCode = m.MemberCode;
                if (Request.Files.Count > 0)
                {
                    var c = Request.Files[0].ContentLength;
                    var reader = new BinaryReader(Request.Files[0].InputStream);
                    member.ProfileImage = reader.ReadBytes(c);
                    reader.Close();
                }
                _logic.AddOrUpdateMembers(member);
                Session["Member"] = member;
                return
                    RedirectToAction("Dashboard");
            }
            catch(Exception ex)
            {
                ViewBag.Error = ex.Message;
                return View(member);
            }
        }
        [HttpGet]
        public ActionResult Gallery()
        {
            var total = 0;
            var m = _logic.GetAllImages(0, int.MaxValue, out total);
            ViewBag.Total = total;
            return
                View(m);
        }

        [HttpPost]
        public ActionResult PostImage(string img64)
        {
            try
            {
                if(img64.StartsWith("data/png:"))
                {
                    img64 = img64.Substring(9);
                }
                var imgBits = Convert.FromBase64String(img64);
                _logic.AddOrUpdateImage(new ImageModel
                {
                    ImageContent = imgBits,
                    Member = ((MemberModel)Session["Member"])
                });
                return Json(new { Success = true }, JsonRequestBehavior.AllowGet);
            }
            catch(Exception ex)
            {
                return
                    Json(new {Error = ex.Message, Success = false }, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        public ActionResult FacebookProfilePicUpload(string imgUrl)
        {
            var client = new WebClient();
            var imgBits = client.DownloadData(imgUrl);
            try
            { 
                _logic.AddOrUpdateImage(new ImageModel
                {
                    ImageContent = imgBits,
                    Member = ((MemberModel)Session["Member"])
                });
                return Json(new { Success = true }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return
                    Json(new { Error = ex.Message, Success = false }, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpGet]
        public ActionResult PostImage()
        {
            return
                View();
        }    
    }
}