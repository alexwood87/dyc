﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WSD.Dyc.BusinessLogic;
using WSD.Dyc.Core.Contracts;
using WSD.Dyc.Domain.Models;
using WSD.Dyc.WebApp.Helpers;

namespace WSD.Dyc.WebApp.Controllers
{
    [SecureContextHeader]
    public class HomeController : Controller
    {
        private IAppBusinessLogic _logic;
        public HomeController()
        {
            _logic = new AppBusinessLogic(ConfigurationManager.ConnectionStrings["ConnString"].ConnectionString);
        }
        [HttpGet]
        public ActionResult Index()
        {
            return View();
        }
        [HttpGet]
        public ActionResult CalendarEvents()
        {
            return
                View();
        }
        [HttpGet]
        public ActionResult CalendarEventDetails(long id)
        {
            return
                View(_logic.SearchEvents(new EventSearchModel
                {
                    EventId = id,
                    PageSize = 1
                }).Single());
        }
       
        [HttpGet]
        public ActionResult LoadCalendarEvents(DateTime start, DateTime end)
        {
            try
            {
                var d = _logic.SearchEvents(new EventSearchModel
                {
                    PageSize = int.MaxValue,
                    FromDate = start,
                    ToDate = end
                }).Select(x => new { id = x.EventId, title = x.Title,
                    start = x.StartDate,
                    end = x.EndDate,
                    url = "/Home/CalendarEventDetails?id=" + x.EventId,
                    allDay = ""
                }).ToArray();
                return Json(d, JsonRequestBehavior.AllowGet);
                /*
                 ["0",
    {
        "allDay": "",
        "title": "Test event",
        "id": "821",
        "end": "2011-06-06 14:00:00",
        "start": "2011-06-06 06:00:00"
    },
    "1",
    {
        "allDay": "",
        "title": "Test event 2",
        "id": "822",
        "end": "2011-06-10 21:00:00",
        "start": "2011-06-10 16:00:00"
    }]
                 */
                //return
                //    Json(new
                //    {
                //        Data = d,
                //        Success = true
                //    }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return
                    Json(new { Error = ex.Message, Success = false}, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpGet]
        public ActionResult MakeReservation()
        {
            return
                View();
        }
       
        [HttpPost]
        public ActionResult MakeReservation(ReservationModel model)
        {
            try
            {
                model.ReservationId = 0;
                _logic.AddUpdateReservations(model);
                return
                    Redirect("~/Home/Confirmation?message=Reservation%20Made");
            }
            catch(ArgumentException ex)
            {
                ViewBag.Error = ex.Message;
                return
                    View(model);
            }
        }
        [HttpGet]
        public ActionResult PostOrder()
        {
            int total;
            ViewBag.Items = _logic.GetAllMenuItems(0, int.MaxValue, out total);
            ViewBag.Total = total;
            return
                View();
        }
        [HttpGet]
        public ActionResult BarAndGalley()
        {
            return
                View();
        }
        [HttpPost]
        public ActionResult PostOrder(OnlineOrderModel order, string hfItems, PaymentModel payment)
        {
            int total = 0;
            try
            {
                if(string.IsNullOrEmpty(hfItems))
                {
                    ViewBag.Error = "You Must Select Some Food Items";
                    ViewBag.Items = _logic.GetAllMenuItems(0, int.MaxValue, out total);
                    return
                        View(order);
                }
                var items = hfItems.Split(',').Where(x => !string.IsNullOrEmpty(x)).Select(x => int.Parse(x)).ToArray();
                order.CreatedDate = DateTime.UtcNow.AddHours(-6);
                order.Items = items.Select(x => new MenuItemModel
                {
                    MenuItemId = x
                }).ToList();
               
                var l = _logic.AddOnlineOrder(order, payment);
                return
                   Redirect("~/Home/ConfirmationSuccess?id=" + l);
            }
            catch(Exception ex)
            {
               
                ViewBag.Error = ex.Message;
                ViewBag.Items = _logic.GetAllMenuItems(0, int.MaxValue, out total);
                return
                    View(order);
            } 
        }
        [HttpGet]
        public ActionResult LoginView()
        {
            return
                View();
        }
        [HttpGet]
        public ActionResult NewsLetter()
        {
           return
                View(_logic.SearchNewsLetters(null, DateTime.UtcNow.AddMonths(-2), 0, int.MaxValue));
        }
        [HttpGet]
        public ActionResult CommodoreNotes()
        {
            return
                 View(_logic.SearchCommodoreNotes(null, DateTime.UtcNow.AddMonths(-2), 0, int.MaxValue));
        }
        [HttpPost]
        public ActionResult MemberLogin(string email, string pin)
        {
            try
            {
                var mem = _logic.ValidateMember(email, pin);
                if (mem != null)
                {
                    Session["Member"] = mem;
                    Session["User"] = _logic.ValidateUser(email, pin);
                    Session["Officer"] = _logic.GetOfficers().SingleOrDefault(x => x.Member.MemberId == mem.MemberId);
                    return
                        Json(new { Success = true, IsUser = Session["User"]!= null, IsOfficer = Session["Officer"] != null }, JsonRequestBehavior.AllowGet);
                }
                return
                    Json(new { Success = false, Error = "Invalid Login Credentials" }, JsonRequestBehavior.AllowGet);
            }
            catch(Exception ex)
            {
                return
                    Json(new { Success = false, Error = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpGet]
        public ActionResult CreateProfile()
        {
            return
                View();
        }
        [HttpPost]
        public ActionResult CreateProfile(MemberModel member)
        {
            try
            {
                member.MemberId = 0;
                member.IsDisabled = true;
                member.MemberCode = "";
                int total;
                var m = _logic.SearchMembers(new MemberSearchModel
                {
                    Email = member.Email,
                    PageSize = 1
                }, out total);
                if (m != null && m.Count > 0)
                {
                    ViewBag.Error = "Email Already Exists";
                    return
                        View(member);
                }
                if(Request.Files.Count > 0)
                {
                    var c = Request.Files[0].ContentLength;
                    var reader = new BinaryReader(Request.Files[0].InputStream);
                    member.ProfileImage = reader.ReadBytes(c);
                    reader.Close();
                }
                member.MemberId = _logic.AddOrUpdateMembers(member);
                Session["Member"] = member;
                return
                    Redirect("~/Home/Index");
            }
            catch(ArgumentException ex)
            {
                ViewBag.Error = ex.Message;
                return View(member);
            }
        }

        [HttpGet]
        public ActionResult PostShoppingOrder()
        {
            int total;
            ViewBag.Items = _logic.GetAllShopItems(0, int.MaxValue, out total);
            ViewBag.Total = total;
            return
                View();
        }
        [HttpPost]
        public ActionResult PostShoppingOrder(ShopOrderModel order, PaymentModel payment, string hfItems)
        {
            int total = 0;
            try
            {
                if(string.IsNullOrEmpty(hfItems))
                {
                    ViewBag.Error = "You Must Have Selected Something To Order";
                    ViewBag.Items = _logic.GetAllShopItems(0, int.MaxValue, out total);
                    return
                        View(order);
                }
                var items = hfItems.Split(',').Where(x => !string.IsNullOrEmpty(x)).Select(x => int.Parse(x)).ToArray();
                order.Items = items.Select(x => new ShopItemModel { ShopItemId = x }).ToList();
                var l = _logic.AddShoppingOrder(order, payment);
                return
                   Redirect("~/Home/ConfirmationSuccess?id=" + l);
            }
            catch(ArgumentException ex)
            {
                ViewBag.Error = ex.Message;
                ViewBag.Items = _logic.GetAllShopItems(0, int.MaxValue, out total);
                return
                    View(order);
            }
        }
        [HttpGet]
        public ActionResult About()
        {
            
            return View();
        }
        [HttpGet]

        public ActionResult Contact()
        {
            
            return View();
        }
        [HttpGet]
        public ActionResult PayDues()
        {
            return
                View();
        }
        [HttpPost]
        public ActionResult PayDues(PaymentModel payment, decimal? amountPaid, string email)
        {
            try
            {
                int total = 0;
                var member = _logic.SearchMembers(new MemberSearchModel
                {
                    Email = email,
                    PageSize = 1,
                }, out total).Single();
                var membershipDue = new MemberDueModel
                {
                    AmountPaid = amountPaid.GetValueOrDefault(0),
                    Member = member,
                    Payment = payment
                };
                var l =_logic.PayDues(membershipDue, payment);
                return
                    Redirect("~/Home/ConfirmationSuccess?id=" + l);
            }
            catch(Exception ex)
            {
                ViewBag.Error = ex.Message;
                return
                    View(payment);
            }
        }
        [HttpGet]
        public ActionResult ConfirmationSuccess(string id)
        {
            ViewBag.ConfirmationMessage = id;
            return
                View();
        }
        [HttpGet]
        public ActionResult ViewOfficers()
        {
            return
                View(_logic.GetOfficers().Where(x => x.TermEnd == null || x.TermEnd.Value > DateTime.UtcNow));
        }
       
    }
}