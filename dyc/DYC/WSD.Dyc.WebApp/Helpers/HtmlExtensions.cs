﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace WSD.Dyc.WebApp.Helpers
{
    public class HtmlExtensions
    {
        public static string ImageToImageTag(byte[] pic, int width, int height, string alt, string name)
        {
            try
            {
                return
                    string.Format("<img src='data:image/png;base64,{0}' alt='{1}' width='{2}' height='{3}' name='{4}' id='{4}'/>", Convert.ToBase64String(pic),
                        alt, width.ToString(), height.ToString(), name);
            }
            catch(FormatException ex)
            {
                return
                    string.Format("<img src='' alt='{0}' />", ex.Message);
            }
        }
        public static string ToCurrencyString(decimal? d)
        {
            if(d == null)
            {
                return
                    "$0.00";
            }
            decimal t = decimal.Truncate(d.Value);
            if (d.Value.Equals(t))
            {
                return "$" + d.Value.ToString("0.##");
            }
            else
            {
                return "$"+ d.Value.ToString("#,##0.00");
            }
        }
        public static List<SelectListItem> AllMembers(int id)
        {
            var logic = new BusinessLogic.AppBusinessLogic(ConfigurationManager.ConnectionStrings["ConnString"].ConnectionString);
            var total = 0;
            return
                logic.SearchMembers(new Domain.Models.MemberSearchModel
                {
                    PageSize = int.MaxValue,
                    IsDisabled = false
                }, out total).Select(x => new SelectListItem
                {
                    Text = x.LastName + "," + x.FirstName + "(" + (x.MemberCode ?? "") + ")",
                    Selected = x.MemberId == id,
                    Value = x.MemberId.ToString()
                }).ToList();
        }
    }
}