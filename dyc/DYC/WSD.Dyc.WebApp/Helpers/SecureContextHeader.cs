﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace WSD.Dyc.WebApp.Helpers
{
    public class SecureContextHeader : AuthorizeAttribute
    {
        public override void OnAuthorization(AuthorizationContext filterContext)
        {
            if(!filterContext.HttpContext.Request.IsSecureConnection && ConfigurationManager.AppSettings["DevMode"] != "Development")
            {
                var path = filterContext.HttpContext.Request.Url.AbsoluteUri;
                path = "https"+path.Substring(4);
                
                filterContext.HttpContext.Response.Redirect(path);
            }
            
        }
    }
}