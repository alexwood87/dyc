﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace WSD.Dyc.WebApp.Helpers
{
    public class AuthorizationHeader : AuthorizeAttribute 
    {
        public override void OnAuthorization(AuthorizationContext filterContext)
        {
            var path = filterContext.HttpContext.Request.Path;
            var session = filterContext.HttpContext.Session;
            var req = filterContext.HttpContext.Response;
            if (path.Contains("Admin") && session["User"] == null)
            {
                req.Redirect("/Home/Index");
                return;
            }
            if(path.Contains("Members") && session["Member"] == null)
            {
                req.Redirect("/Home/Index");
                return;
            }
        }
    }
}