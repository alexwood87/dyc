﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WSD.Dyc.Domain.Helpers
{
    public class FoodCategories
    {
        public static string DESERT = "Desert", DINNER = "Dinner", LUNCH = "Lunch", LUNCH_SPECIAL = "Lunch Special",
            BREAKFAST = "Breakfast", BREAKFAST_SPECIAL = "Breakfast Special", DINNER_SPECIAL = "Dinner Special",
            APPETIZERS = "Appetizers", DRINKS = "Drinks", ALCOHOLIC_DRINKS= "Alcoholic Drinks";
        public static List<string> Categories = new List<string>
        {
            APPETIZERS, BREAKFAST, BREAKFAST_SPECIAL, LUNCH, LUNCH_SPECIAL, DINNER, DINNER_SPECIAL, DESERT,
            ALCOHOLIC_DRINKS, DRINKS
        };
    }
}
