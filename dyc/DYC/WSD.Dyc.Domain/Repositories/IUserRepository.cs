﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WSD.Dyc.Domain.Models;

namespace WSD.Dyc.Domain.Repositories
{
    public interface IUserRepository
    {
        UserModel ValidateUser(string email, string pin);
        List<UserModel> GetAllUsers();
        void RemoveUser(int userId);
        int AddUpdateUser(UserModel u);
    }
}
