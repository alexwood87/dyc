﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WSD.Dyc.Domain.Models;

namespace WSD.Dyc.Domain.Repositories
{
    public interface IShoppingRepository
    {
        long AddShoppingOrder(ShopOrderModel model);
        long AddOnlineOrder(OnlineOrderModel model);
        void InvalidateOnlineOrder(long orderId);
        void InvalidateShopOrder(long orderId);
        List<OnlineOrderModel> SearchOnlineOrders(ShoppingSearchModel search);
        List<ShopOrderModel> SearchShoppingOrders(ShoppingSearchModel search);
        int AddUpdateMenuItem(MenuItemModel item);
        int AddUpdateShoppingItem(ShopItemModel item);
        List<ShopItemModel> GetAllShopItems(int pageIndex, int pageSize, out int total);
        List<MenuItemModel> GetAllMenuItems(int pageIndex, int pageSize, out int total);
        List<ShopItemModel> GetAllShopItemsAdmin(int pageIndex, int pageSize, out int total);
        List<MenuItemModel> GetAllMenuItemsAdmin(int pageIndex, int pageSize, out int total);
        List<ReservationModel> SearchReservations(ReservationSearch search);
        long AddUpdateReservations(ReservationModel model);
        List<OnlineOrderModel> FindBarGrillOrders(DateTime? dateFulfilled);
        List<ShopOrderModel> FindShoppingOrders(DateTime? dateFulfilled);
    }
}
