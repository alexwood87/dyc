﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WSD.Dyc.Domain.Models;

namespace WSD.Dyc.Domain.Repositories
{
    public interface IMemberRepository
    {
        List<MemberModel> SearchMembers(MemberSearchModel search, out int total);
        MemberModel ValidateMember(string email, string pin);
        int AddOrUpdateMembers(MemberModel model);
        List<ClubOfficerModel> GetOfficers();
        ClubOfficerModel IsClubOfficer(int memberId, DateTime termDate);
        int AddOrUpdateOfficer(ClubOfficerModel model);
        long PayDues(MemberDueModel model);
    }
}
