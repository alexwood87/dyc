﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WSD.Dyc.Domain.Models;

namespace WSD.Dyc.Domain.Repositories
{
    public interface IImageRepository
    {
        List<ImageModel> GetAllImages(int pageIndex, int pageSize, out int total);
        int AddOrUpdateImage(ImageModel model);

    }
}
