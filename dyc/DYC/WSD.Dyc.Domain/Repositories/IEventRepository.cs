﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WSD.Dyc.Domain.Models;

namespace WSD.Dyc.Domain.Repositories
{
    public interface IEventRepository
    {
        long AddUpdateEvent(CalendarEventModel model);
        List<CalendarEventModel> SearchEvents(EventSearchModel search);
    }
}
