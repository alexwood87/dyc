﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WSD.Dyc.Domain.Models;
namespace WSD.Dyc.Domain.Repositories
{
    public interface INotesAndNewsRepository
    {
        List<CommodoreNoteModel> SearchCommodoreNotes(int? id, DateTime? currentDate, int pageIndex, int pageSize);
        int AddUpdateCommodoreNote(CommodoreNoteModel note);
        List<NewsLetterModel> SearchNewsLetters(int? id, DateTime? currentDate, int pageIndex, int pageSize);
        int AddUpdateNewsLetter(NewsLetterModel model);
    }
}
