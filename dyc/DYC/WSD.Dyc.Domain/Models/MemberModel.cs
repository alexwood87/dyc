﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace WSD.Dyc.Domain.Models
{
    [DataContract]
    public class MemberModel
    {
        [DataMember]
        public bool OptOut { get; set; }
        [DataMember]
        public int MemberId { get; set; }
        [DataMember]
        public string Email { get; set; }
        [DataMember]
        public string Pin { get; set; }
        [DataMember]
        public string FirstName { get; set; }
        [DataMember]
        public string LastName { get; set; }
        [DataMember]
        public string MiddleName { get; set; }
        [DataMember]
        public string MemberCode { get; set; }
        [DataMember]
        public bool IsDisabled { get; set; }
        [DataMember]
        public byte[] ProfileImage { get; set; }
    }
}
