﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace WSD.Dyc.Domain.Models
{
    [DataContract]
    public class MemberSearchModel
    {
        [DataMember]
        public bool? HasOptedOut { get; set; }
        [DataMember]
        public int? MemberId { get; set; }
        [DataMember]
        public string LastName { get; set; }
        [DataMember]
        public string FirstName { get; set; }
        [DataMember]
        public string MemberCode { get; set; }
        [DataMember]
        public string Email { get; set; }
        [DataMember]
        public bool? IsDisabled { get; set; }
        [DataMember]
        public int PageIndex { get; set; }
        [DataMember]
        public int PageSize { get; set; }
    }
}
