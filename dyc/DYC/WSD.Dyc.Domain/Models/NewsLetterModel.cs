﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace WSD.Dyc.Domain.Models
{
    [DataContract]
    public class NewsLetterModel
    {
        [DataMember]
        public int NewsLetterId { get; set; }
        [DataMember]
        public DateTime CreatedDate { get; set; }
        [DataMember]
        public string Content { get; set; }
        [DataMember]
        public string Title { get; set; }
    }
}
