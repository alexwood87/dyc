﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace WSD.Dyc.Domain.Models
{
    [DataContract]
    public class MemberDueModel
    {
        [DataMember]
        public long MemberDueId { get; set; }
        [DataMember]
        public MemberModel Member { get; set; }
        [DataMember]
        public decimal AmountPaid { get; set; }
        [DataMember]
        public PaymentModel Payment { get; set; }
    }
}
