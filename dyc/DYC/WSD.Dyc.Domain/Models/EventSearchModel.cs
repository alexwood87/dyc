﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace WSD.Dyc.Domain.Models
{
    [DataContract]
    public class EventSearchModel
    {
        [DataMember]
        public long? EventId { get; set; }
        [DataMember]
        public string Title { get; set; }
        [DataMember]
        public string Description { get; set; }
        [DataMember]
        public DateTime? ToDate { get; set; }
        [DataMember]
        public DateTime? FromDate { get; set; }
        [DataMember]
        public int PageIndex { get; set;}
        [DataMember]
        public int PageSize { get; set; }
    }
}
