﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace WSD.Dyc.Domain.Models
{
    [DataContract]
    public class ShoppingSearchModel
    {
        [DataMember]
        public long? OrderId { get; set; }
        [DataMember]
        public string OrderName { get; set; }
        [DataMember]
        public DateTime? OrderDate { get; set; }
        [DataMember]
        public int PageIndex { get; set; }
        [DataMember]
        public int PageSize { get; set; }
    }
}
