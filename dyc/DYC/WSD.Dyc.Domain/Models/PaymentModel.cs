﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace WSD.Dyc.Domain.Models
{
    [DataContract]
    public class PaymentModel
    {
        [DataMember]
        public string CreditCardNumber { get; set; }
        [DataMember]
        public string ExperationYear { get; set; }
        [DataMember]
        public string ExperationMonth { get; set; }
        [DataMember]
        public string CVVCode { get; set; }
        [DataMember]
        public string CardType { get; set; }
        [DataMember]
        public string CardZipCode { get; set; }
        [DataMember]
        public bool IsPaypal { get; set; }
        [DataMember]
        public string NameOnCard { get; set; }
    }
}
