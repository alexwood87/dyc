﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace WSD.Dyc.Domain.Models
{
    [DataContract]
    public class ReservationSearch
    {
        [DataMember]
        public long? ReservationId { get; set; }
        [DataMember]
        public DateTime? ToDate { get; set; }
        [DataMember]
        public DateTime? FromDate { get; set; }
        [DataMember]
        public string GroupName { get; set; }
        [DataMember]
        public int PageSize { get; set; }
        [DataMember]
        public int PageIndex { get; set; }

    }
}
