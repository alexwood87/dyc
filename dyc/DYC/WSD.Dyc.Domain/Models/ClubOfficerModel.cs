﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace WSD.Dyc.Domain.Models
{
    [DataContract]
    public class ClubOfficerModel
    {
        [DataMember]
        public int OfficerId { get; set; }
        [DataMember]
        public string Title { get; set; }
        [DataMember]
        public MemberModel Member { get; set; }
        [DataMember]
        public DateTime TermStart { get; set; }
        [DataMember]
        public DateTime? TermEnd { get; set; }
    }
}
