﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace WSD.Dyc.Domain.Models
{
    [DataContract]
    public class ReservationModel
    {
        [DataMember]
        public long ReservationId { get; set; }
        [DataMember]
        public string GroupName { get; set; }
        [DataMember]
        public DateTime CreatedDate { get; set;}
        [DataMember]
        public int NumberOfGuests { get; set; }
        [DataMember]
        public DateTime ReservationDate { get; set; }
    }
}
