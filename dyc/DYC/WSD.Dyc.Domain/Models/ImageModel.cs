﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace WSD.Dyc.Domain.Models
{
    [DataContract]
    public class ImageModel
    {
        [DataMember]
        public int ImageId { get; set; }
        [DataMember]
        public byte[] ImageContent { get; set; }
        [DataMember]
        public MemberModel Member { get; set; }
    }
}
