﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace WSD.Dyc.Domain.Models
{
    [DataContract]
    public class CommodoreNoteModel
    {
        [DataMember]
        public int NoteId { get; set; }
        [DataMember]
        public string Note { get; set; }
        [DataMember]
        public DateTime CreatedDate { get; set; }
    }
}
