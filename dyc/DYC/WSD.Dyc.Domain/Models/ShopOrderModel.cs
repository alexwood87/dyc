﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace WSD.Dyc.Domain.Models
{
    [DataContract]
    public class ShopOrderModel
    {
        [DataMember]
        public PaymentModel Payment { get; set; }

        [DataMember]
        public long OrderId { get; set; }
        [DataMember]
        public string OrderName { get; set; }
        [DataMember]
        public DateTime OrderDate { get; set; }
        [DataMember]
        public DateTime CreatedDate { get; set; }
        [DataMember]
        public string Phone { get; set; }
        [DataMember]
        public string Address1 { get; set; }
        [DataMember]
        public string Address2 { get; set; }
        [DataMember]
        public string City { get; set; }
        [DataMember]
        public string StateOrProvidence { get; set; }
        [DataMember]
        public string ZipCode { get; set; }
        [DataMember]
        public string Dock { get; set; }
        [DataMember]
        public string SlipNumber { get; set; }
        [DataMember]
        public string VesselName { get; set; }
        [DataMember]
        public DateTime? DateFulfilled { get; set; }
        [DataMember]
        public List<ShopItemModel> Items { get; set; }
    }
}
