﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WSD.Dyc.Domain.Repositories;

namespace WSD.Dyc.Domain.UnitsOfWork
{
    public interface IUnitOfWork
    {
        INotesAndNewsRepository NotesAndNewsRepository { get; set; }
        IShoppingRepository ShoppingRepository { get; set; }
        IMemberRepository MemberRepository { get; set; }
        IUserRepository UserRepository { get; set; }
        IImageRepository ImageRepository { get; set; }
        IEventRepository EventRepository { get; set; }
    }
}
